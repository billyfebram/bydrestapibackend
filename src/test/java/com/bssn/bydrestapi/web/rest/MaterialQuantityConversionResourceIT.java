package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialQuantityConversion;
import com.bssn.bydrestapi.repository.MaterialQuantityConversionRepository;
import com.bssn.bydrestapi.service.MaterialQuantityConversionService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;

import java.math.BigDecimal;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialQuantityConversionResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialQuantityConversionResourceIT {

    private static final BigDecimal DEFAULT_CORRESPONDING_QUANTITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_CORRESPONDING_QUANTITY = new BigDecimal(2);

    private static final String DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT = "BBBBBBBBBB";

    private static final BigDecimal DEFAULT_QUANTITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_QUANTITY = new BigDecimal(2);

    private static final String DEFAULT_QUANTITY_UNIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_QUANTITY_UNIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_QUANTITY_UNIT_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_QUANTITY_UNIT_CODE_TEXT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_BATCH_DEPENDENT_INDICATOR = false;
    private static final Boolean UPDATED_BATCH_DEPENDENT_INDICATOR = true;

    @Autowired
    private MaterialQuantityConversionRepository materialQuantityConversionRepository;

    @Autowired
    private MaterialQuantityConversionService materialQuantityConversionService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialQuantityConversionMockMvc;

    private MaterialQuantityConversion materialQuantityConversion;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialQuantityConversionResource materialQuantityConversionResource = new MaterialQuantityConversionResource(materialQuantityConversionService);
        this.restMaterialQuantityConversionMockMvc = MockMvcBuilders.standaloneSetup(materialQuantityConversionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialQuantityConversion createEntity(EntityManager em) {
        MaterialQuantityConversion materialQuantityConversion = new MaterialQuantityConversion()
            .correspondingQuantity(DEFAULT_CORRESPONDING_QUANTITY)
            .correspondingQuantityUnitCode(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE)
            .correspondingQuantityUnitCodeText(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT)
            .quantity(DEFAULT_QUANTITY)
            .quantityUnitCode(DEFAULT_QUANTITY_UNIT_CODE)
            .quantityUnitCodeText(DEFAULT_QUANTITY_UNIT_CODE_TEXT)
            .batchDependentIndicator(DEFAULT_BATCH_DEPENDENT_INDICATOR);
        return materialQuantityConversion;
    }

    @BeforeEach
    public void initTest() {
        materialQuantityConversion = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialQuantityConversion() throws Exception {
        int databaseSizeBeforeCreate = materialQuantityConversionRepository.findAll().size();

        // Create the MaterialQuantityConversion
        restMaterialQuantityConversionMockMvc.perform(post("/api/material-quantity-conversions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuantityConversion)))
            .andExpect(status().isCreated());

        // Validate the MaterialQuantityConversion in the database
        List<MaterialQuantityConversion> materialQuantityConversionList = materialQuantityConversionRepository.findAll();
        assertThat(materialQuantityConversionList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialQuantityConversion testMaterialQuantityConversion = materialQuantityConversionList.get(materialQuantityConversionList.size() - 1);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantity()).isEqualTo(DEFAULT_CORRESPONDING_QUANTITY);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantityUnitCode()).isEqualTo(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantityUnitCodeText()).isEqualTo(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT);
        assertThat(testMaterialQuantityConversion.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testMaterialQuantityConversion.getQuantityUnitCode()).isEqualTo(DEFAULT_QUANTITY_UNIT_CODE);
        assertThat(testMaterialQuantityConversion.getQuantityUnitCodeText()).isEqualTo(DEFAULT_QUANTITY_UNIT_CODE_TEXT);
        assertThat(testMaterialQuantityConversion.isBatchDependentIndicator()).isEqualTo(DEFAULT_BATCH_DEPENDENT_INDICATOR);
    }

    @Test
    @Transactional
    public void createMaterialQuantityConversionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialQuantityConversionRepository.findAll().size();

        // Create the MaterialQuantityConversion with an existing ID
        materialQuantityConversion.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialQuantityConversionMockMvc.perform(post("/api/material-quantity-conversions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuantityConversion)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialQuantityConversion in the database
        List<MaterialQuantityConversion> materialQuantityConversionList = materialQuantityConversionRepository.findAll();
        assertThat(materialQuantityConversionList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialQuantityConversions() throws Exception {
        // Initialize the database
        materialQuantityConversionRepository.saveAndFlush(materialQuantityConversion);

        // Get all the materialQuantityConversionList
        restMaterialQuantityConversionMockMvc.perform(get("/api/material-quantity-conversions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialQuantityConversion.getId().intValue())))
            .andExpect(jsonPath("$.[*].correspondingQuantity").value(hasItem(DEFAULT_CORRESPONDING_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].correspondingQuantityUnitCode").value(hasItem(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE.toString())))
            .andExpect(jsonPath("$.[*].correspondingQuantityUnitCodeText").value(hasItem(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY.doubleValue())))
            .andExpect(jsonPath("$.[*].quantityUnitCode").value(hasItem(DEFAULT_QUANTITY_UNIT_CODE.toString())))
            .andExpect(jsonPath("$.[*].quantityUnitCodeText").value(hasItem(DEFAULT_QUANTITY_UNIT_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].batchDependentIndicator").value(hasItem(DEFAULT_BATCH_DEPENDENT_INDICATOR.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getMaterialQuantityConversion() throws Exception {
        // Initialize the database
        materialQuantityConversionRepository.saveAndFlush(materialQuantityConversion);

        // Get the materialQuantityConversion
        restMaterialQuantityConversionMockMvc.perform(get("/api/material-quantity-conversions/{id}", materialQuantityConversion.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialQuantityConversion.getId().intValue()))
            .andExpect(jsonPath("$.correspondingQuantity").value(DEFAULT_CORRESPONDING_QUANTITY.doubleValue()))
            .andExpect(jsonPath("$.correspondingQuantityUnitCode").value(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE.toString()))
            .andExpect(jsonPath("$.correspondingQuantityUnitCodeText").value(DEFAULT_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY.doubleValue()))
            .andExpect(jsonPath("$.quantityUnitCode").value(DEFAULT_QUANTITY_UNIT_CODE.toString()))
            .andExpect(jsonPath("$.quantityUnitCodeText").value(DEFAULT_QUANTITY_UNIT_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.batchDependentIndicator").value(DEFAULT_BATCH_DEPENDENT_INDICATOR.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialQuantityConversion() throws Exception {
        // Get the materialQuantityConversion
        restMaterialQuantityConversionMockMvc.perform(get("/api/material-quantity-conversions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialQuantityConversion() throws Exception {
        // Initialize the database
        materialQuantityConversionService.save(materialQuantityConversion);

        int databaseSizeBeforeUpdate = materialQuantityConversionRepository.findAll().size();

        // Update the materialQuantityConversion
        MaterialQuantityConversion updatedMaterialQuantityConversion = materialQuantityConversionRepository.findById(materialQuantityConversion.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialQuantityConversion are not directly saved in db
        em.detach(updatedMaterialQuantityConversion);
        updatedMaterialQuantityConversion
            .correspondingQuantity(UPDATED_CORRESPONDING_QUANTITY)
            .correspondingQuantityUnitCode(UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE)
            .correspondingQuantityUnitCodeText(UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT)
            .quantity(UPDATED_QUANTITY)
            .quantityUnitCode(UPDATED_QUANTITY_UNIT_CODE)
            .quantityUnitCodeText(UPDATED_QUANTITY_UNIT_CODE_TEXT)
            .batchDependentIndicator(UPDATED_BATCH_DEPENDENT_INDICATOR);

        restMaterialQuantityConversionMockMvc.perform(put("/api/material-quantity-conversions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialQuantityConversion)))
            .andExpect(status().isOk());

        // Validate the MaterialQuantityConversion in the database
        List<MaterialQuantityConversion> materialQuantityConversionList = materialQuantityConversionRepository.findAll();
        assertThat(materialQuantityConversionList).hasSize(databaseSizeBeforeUpdate);
        MaterialQuantityConversion testMaterialQuantityConversion = materialQuantityConversionList.get(materialQuantityConversionList.size() - 1);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantity()).isEqualTo(UPDATED_CORRESPONDING_QUANTITY);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantityUnitCode()).isEqualTo(UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE);
        assertThat(testMaterialQuantityConversion.getCorrespondingQuantityUnitCodeText()).isEqualTo(UPDATED_CORRESPONDING_QUANTITY_UNIT_CODE_TEXT);
        assertThat(testMaterialQuantityConversion.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testMaterialQuantityConversion.getQuantityUnitCode()).isEqualTo(UPDATED_QUANTITY_UNIT_CODE);
        assertThat(testMaterialQuantityConversion.getQuantityUnitCodeText()).isEqualTo(UPDATED_QUANTITY_UNIT_CODE_TEXT);
        assertThat(testMaterialQuantityConversion.isBatchDependentIndicator()).isEqualTo(UPDATED_BATCH_DEPENDENT_INDICATOR);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialQuantityConversion() throws Exception {
        int databaseSizeBeforeUpdate = materialQuantityConversionRepository.findAll().size();

        // Create the MaterialQuantityConversion

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialQuantityConversionMockMvc.perform(put("/api/material-quantity-conversions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialQuantityConversion)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialQuantityConversion in the database
        List<MaterialQuantityConversion> materialQuantityConversionList = materialQuantityConversionRepository.findAll();
        assertThat(materialQuantityConversionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialQuantityConversion() throws Exception {
        // Initialize the database
        materialQuantityConversionService.save(materialQuantityConversion);

        int databaseSizeBeforeDelete = materialQuantityConversionRepository.findAll().size();

        // Delete the materialQuantityConversion
        restMaterialQuantityConversionMockMvc.perform(delete("/api/material-quantity-conversions/{id}", materialQuantityConversion.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialQuantityConversion> materialQuantityConversionList = materialQuantityConversionRepository.findAll();
        assertThat(materialQuantityConversionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialQuantityConversion.class);
        MaterialQuantityConversion materialQuantityConversion1 = new MaterialQuantityConversion();
        materialQuantityConversion1.setId(1L);
        MaterialQuantityConversion materialQuantityConversion2 = new MaterialQuantityConversion();
        materialQuantityConversion2.setId(materialQuantityConversion1.getId());
        assertThat(materialQuantityConversion1).isEqualTo(materialQuantityConversion2);
        materialQuantityConversion2.setId(2L);
        assertThat(materialQuantityConversion1).isNotEqualTo(materialQuantityConversion2);
        materialQuantityConversion1.setId(null);
        assertThat(materialQuantityConversion1).isNotEqualTo(materialQuantityConversion2);
    }
}
