package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation;
import com.bssn.bydrestapi.repository.MaterialSupplyPlanningProcessInformationRepository;
import com.bssn.bydrestapi.service.MaterialSupplyPlanningProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialSupplyPlanningProcessInformationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialSupplyPlanningProcessInformationResourceIT {

    private static final String DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_LOT_SIZE_PROCEDURE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LOT_SIZE_PROCEDURE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LOT_SIZE_PROCEDURE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LOT_SIZE_PROCEDURE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLY_PLANNING_AREA_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLY_PLANNING_AREA_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialSupplyPlanningProcessInformationRepository materialSupplyPlanningProcessInformationRepository;

    @Autowired
    private MaterialSupplyPlanningProcessInformationService materialSupplyPlanningProcessInformationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialSupplyPlanningProcessInformationMockMvc;

    private MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialSupplyPlanningProcessInformationResource materialSupplyPlanningProcessInformationResource = new MaterialSupplyPlanningProcessInformationResource(materialSupplyPlanningProcessInformationService);
        this.restMaterialSupplyPlanningProcessInformationMockMvc = MockMvcBuilders.standaloneSetup(materialSupplyPlanningProcessInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialSupplyPlanningProcessInformation createEntity(EntityManager em) {
        MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation = new MaterialSupplyPlanningProcessInformation()
            .defaultProcurementMethodCode(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE)
            .defaultProcurementMethodCodeText(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT)
            .supplyPlanningProcedureCode(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE)
            .supplyPlanningProcedureCodeText(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT)
            .lotSizeProcedureCode(DEFAULT_LOT_SIZE_PROCEDURE_CODE)
            .lotSizeProcedureCodeText(DEFAULT_LOT_SIZE_PROCEDURE_CODE_TEXT)
            .supplyPlanningAreaID(DEFAULT_SUPPLY_PLANNING_AREA_ID)
            .lifeCycleStatusCode(DEFAULT_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
        return materialSupplyPlanningProcessInformation;
    }

    @BeforeEach
    public void initTest() {
        materialSupplyPlanningProcessInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialSupplyPlanningProcessInformation() throws Exception {
        int databaseSizeBeforeCreate = materialSupplyPlanningProcessInformationRepository.findAll().size();

        // Create the MaterialSupplyPlanningProcessInformation
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(post("/api/material-supply-planning-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSupplyPlanningProcessInformation)))
            .andExpect(status().isCreated());

        // Validate the MaterialSupplyPlanningProcessInformation in the database
        List<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformationList = materialSupplyPlanningProcessInformationRepository.findAll();
        assertThat(materialSupplyPlanningProcessInformationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialSupplyPlanningProcessInformation testMaterialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformationList.get(materialSupplyPlanningProcessInformationList.size() - 1);
        assertThat(testMaterialSupplyPlanningProcessInformation.getDefaultProcurementMethodCode()).isEqualTo(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getDefaultProcurementMethodCodeText()).isEqualTo(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningProcedureCode()).isEqualTo(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningProcedureCodeText()).isEqualTo(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLotSizeProcedureCode()).isEqualTo(DEFAULT_LOT_SIZE_PROCEDURE_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLotSizeProcedureCodeText()).isEqualTo(DEFAULT_LOT_SIZE_PROCEDURE_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningAreaID()).isEqualTo(DEFAULT_SUPPLY_PLANNING_AREA_ID);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLifeCycleStatusCode()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialSupplyPlanningProcessInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialSupplyPlanningProcessInformationRepository.findAll().size();

        // Create the MaterialSupplyPlanningProcessInformation with an existing ID
        materialSupplyPlanningProcessInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(post("/api/material-supply-planning-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSupplyPlanningProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialSupplyPlanningProcessInformation in the database
        List<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformationList = materialSupplyPlanningProcessInformationRepository.findAll();
        assertThat(materialSupplyPlanningProcessInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialSupplyPlanningProcessInformations() throws Exception {
        // Initialize the database
        materialSupplyPlanningProcessInformationRepository.saveAndFlush(materialSupplyPlanningProcessInformation);

        // Get all the materialSupplyPlanningProcessInformationList
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(get("/api/material-supply-planning-process-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialSupplyPlanningProcessInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].defaultProcurementMethodCode").value(hasItem(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE.toString())))
            .andExpect(jsonPath("$.[*].defaultProcurementMethodCodeText").value(hasItem(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].supplyPlanningProcedureCode").value(hasItem(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE.toString())))
            .andExpect(jsonPath("$.[*].supplyPlanningProcedureCodeText").value(hasItem(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].lotSizeProcedureCode").value(hasItem(DEFAULT_LOT_SIZE_PROCEDURE_CODE.toString())))
            .andExpect(jsonPath("$.[*].lotSizeProcedureCodeText").value(hasItem(DEFAULT_LOT_SIZE_PROCEDURE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].supplyPlanningAreaID").value(hasItem(DEFAULT_SUPPLY_PLANNING_AREA_ID.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCode").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCodeText").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialSupplyPlanningProcessInformation() throws Exception {
        // Initialize the database
        materialSupplyPlanningProcessInformationRepository.saveAndFlush(materialSupplyPlanningProcessInformation);

        // Get the materialSupplyPlanningProcessInformation
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(get("/api/material-supply-planning-process-informations/{id}", materialSupplyPlanningProcessInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialSupplyPlanningProcessInformation.getId().intValue()))
            .andExpect(jsonPath("$.defaultProcurementMethodCode").value(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE.toString()))
            .andExpect(jsonPath("$.defaultProcurementMethodCodeText").value(DEFAULT_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.supplyPlanningProcedureCode").value(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE.toString()))
            .andExpect(jsonPath("$.supplyPlanningProcedureCodeText").value(DEFAULT_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.lotSizeProcedureCode").value(DEFAULT_LOT_SIZE_PROCEDURE_CODE.toString()))
            .andExpect(jsonPath("$.lotSizeProcedureCodeText").value(DEFAULT_LOT_SIZE_PROCEDURE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.supplyPlanningAreaID").value(DEFAULT_SUPPLY_PLANNING_AREA_ID.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCode").value(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCodeText").value(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialSupplyPlanningProcessInformation() throws Exception {
        // Get the materialSupplyPlanningProcessInformation
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(get("/api/material-supply-planning-process-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialSupplyPlanningProcessInformation() throws Exception {
        // Initialize the database
        materialSupplyPlanningProcessInformationService.save(materialSupplyPlanningProcessInformation);

        int databaseSizeBeforeUpdate = materialSupplyPlanningProcessInformationRepository.findAll().size();

        // Update the materialSupplyPlanningProcessInformation
        MaterialSupplyPlanningProcessInformation updatedMaterialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformationRepository.findById(materialSupplyPlanningProcessInformation.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialSupplyPlanningProcessInformation are not directly saved in db
        em.detach(updatedMaterialSupplyPlanningProcessInformation);
        updatedMaterialSupplyPlanningProcessInformation
            .defaultProcurementMethodCode(UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE)
            .defaultProcurementMethodCodeText(UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT)
            .supplyPlanningProcedureCode(UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE)
            .supplyPlanningProcedureCodeText(UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT)
            .lotSizeProcedureCode(UPDATED_LOT_SIZE_PROCEDURE_CODE)
            .lotSizeProcedureCodeText(UPDATED_LOT_SIZE_PROCEDURE_CODE_TEXT)
            .supplyPlanningAreaID(UPDATED_SUPPLY_PLANNING_AREA_ID)
            .lifeCycleStatusCode(UPDATED_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);

        restMaterialSupplyPlanningProcessInformationMockMvc.perform(put("/api/material-supply-planning-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialSupplyPlanningProcessInformation)))
            .andExpect(status().isOk());

        // Validate the MaterialSupplyPlanningProcessInformation in the database
        List<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformationList = materialSupplyPlanningProcessInformationRepository.findAll();
        assertThat(materialSupplyPlanningProcessInformationList).hasSize(databaseSizeBeforeUpdate);
        MaterialSupplyPlanningProcessInformation testMaterialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformationList.get(materialSupplyPlanningProcessInformationList.size() - 1);
        assertThat(testMaterialSupplyPlanningProcessInformation.getDefaultProcurementMethodCode()).isEqualTo(UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getDefaultProcurementMethodCodeText()).isEqualTo(UPDATED_DEFAULT_PROCUREMENT_METHOD_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningProcedureCode()).isEqualTo(UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningProcedureCodeText()).isEqualTo(UPDATED_SUPPLY_PLANNING_PROCEDURE_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLotSizeProcedureCode()).isEqualTo(UPDATED_LOT_SIZE_PROCEDURE_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLotSizeProcedureCodeText()).isEqualTo(UPDATED_LOT_SIZE_PROCEDURE_CODE_TEXT);
        assertThat(testMaterialSupplyPlanningProcessInformation.getSupplyPlanningAreaID()).isEqualTo(UPDATED_SUPPLY_PLANNING_AREA_ID);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLifeCycleStatusCode()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialSupplyPlanningProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialSupplyPlanningProcessInformation() throws Exception {
        int databaseSizeBeforeUpdate = materialSupplyPlanningProcessInformationRepository.findAll().size();

        // Create the MaterialSupplyPlanningProcessInformation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(put("/api/material-supply-planning-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSupplyPlanningProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialSupplyPlanningProcessInformation in the database
        List<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformationList = materialSupplyPlanningProcessInformationRepository.findAll();
        assertThat(materialSupplyPlanningProcessInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialSupplyPlanningProcessInformation() throws Exception {
        // Initialize the database
        materialSupplyPlanningProcessInformationService.save(materialSupplyPlanningProcessInformation);

        int databaseSizeBeforeDelete = materialSupplyPlanningProcessInformationRepository.findAll().size();

        // Delete the materialSupplyPlanningProcessInformation
        restMaterialSupplyPlanningProcessInformationMockMvc.perform(delete("/api/material-supply-planning-process-informations/{id}", materialSupplyPlanningProcessInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformationList = materialSupplyPlanningProcessInformationRepository.findAll();
        assertThat(materialSupplyPlanningProcessInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialSupplyPlanningProcessInformation.class);
        MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation1 = new MaterialSupplyPlanningProcessInformation();
        materialSupplyPlanningProcessInformation1.setId(1L);
        MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation2 = new MaterialSupplyPlanningProcessInformation();
        materialSupplyPlanningProcessInformation2.setId(materialSupplyPlanningProcessInformation1.getId());
        assertThat(materialSupplyPlanningProcessInformation1).isEqualTo(materialSupplyPlanningProcessInformation2);
        materialSupplyPlanningProcessInformation2.setId(2L);
        assertThat(materialSupplyPlanningProcessInformation1).isNotEqualTo(materialSupplyPlanningProcessInformation2);
        materialSupplyPlanningProcessInformation1.setId(null);
        assertThat(materialSupplyPlanningProcessInformation1).isNotEqualTo(materialSupplyPlanningProcessInformation2);
    }
}
