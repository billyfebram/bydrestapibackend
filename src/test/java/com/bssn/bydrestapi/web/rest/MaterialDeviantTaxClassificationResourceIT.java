package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification;
import com.bssn.bydrestapi.repository.MaterialDeviantTaxClassificationRepository;
import com.bssn.bydrestapi.service.MaterialDeviantTaxClassificationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialDeviantTaxClassificationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialDeviantTaxClassificationResourceIT {

    private static final String DEFAULT_COUNTRY_CODE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_CODE = "AAAAAAAAAA";
    private static final String UPDATED_REGION_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_REGION_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_REGION_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_EXEMPTION_REASON_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TAX_EXEMPTION_REASON_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_EXEMPTION_REASON_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TAX_EXEMPTION_REASON_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_RATE_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TAX_RATE_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_RATE_TYPE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TAX_RATE_TYPE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_TAX_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_TAX_TYPE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TAX_TYPE_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialDeviantTaxClassificationRepository materialDeviantTaxClassificationRepository;

    @Autowired
    private MaterialDeviantTaxClassificationService materialDeviantTaxClassificationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialDeviantTaxClassificationMockMvc;

    private MaterialDeviantTaxClassification materialDeviantTaxClassification;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialDeviantTaxClassificationResource materialDeviantTaxClassificationResource = new MaterialDeviantTaxClassificationResource(materialDeviantTaxClassificationService);
        this.restMaterialDeviantTaxClassificationMockMvc = MockMvcBuilders.standaloneSetup(materialDeviantTaxClassificationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialDeviantTaxClassification createEntity(EntityManager em) {
        MaterialDeviantTaxClassification materialDeviantTaxClassification = new MaterialDeviantTaxClassification()
            .countryCode(DEFAULT_COUNTRY_CODE)
            .countryCodeText(DEFAULT_COUNTRY_CODE_TEXT)
            .regionCode(DEFAULT_REGION_CODE)
            .regionCodeText(DEFAULT_REGION_CODE_TEXT)
            .taxExemptionReasonCode(DEFAULT_TAX_EXEMPTION_REASON_CODE)
            .taxExemptionReasonCodeText(DEFAULT_TAX_EXEMPTION_REASON_CODE_TEXT)
            .taxRateTypeCode(DEFAULT_TAX_RATE_TYPE_CODE)
            .taxRateTypeCodeText(DEFAULT_TAX_RATE_TYPE_CODE_TEXT)
            .taxTypeCode(DEFAULT_TAX_TYPE_CODE)
            .taxTypeCodeText(DEFAULT_TAX_TYPE_CODE_TEXT);
        return materialDeviantTaxClassification;
    }

    @BeforeEach
    public void initTest() {
        materialDeviantTaxClassification = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialDeviantTaxClassification() throws Exception {
        int databaseSizeBeforeCreate = materialDeviantTaxClassificationRepository.findAll().size();

        // Create the MaterialDeviantTaxClassification
        restMaterialDeviantTaxClassificationMockMvc.perform(post("/api/material-deviant-tax-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialDeviantTaxClassification)))
            .andExpect(status().isCreated());

        // Validate the MaterialDeviantTaxClassification in the database
        List<MaterialDeviantTaxClassification> materialDeviantTaxClassificationList = materialDeviantTaxClassificationRepository.findAll();
        assertThat(materialDeviantTaxClassificationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialDeviantTaxClassification testMaterialDeviantTaxClassification = materialDeviantTaxClassificationList.get(materialDeviantTaxClassificationList.size() - 1);
        assertThat(testMaterialDeviantTaxClassification.getCountryCode()).isEqualTo(DEFAULT_COUNTRY_CODE);
        assertThat(testMaterialDeviantTaxClassification.getCountryCodeText()).isEqualTo(DEFAULT_COUNTRY_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getRegionCode()).isEqualTo(DEFAULT_REGION_CODE);
        assertThat(testMaterialDeviantTaxClassification.getRegionCodeText()).isEqualTo(DEFAULT_REGION_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxExemptionReasonCode()).isEqualTo(DEFAULT_TAX_EXEMPTION_REASON_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxExemptionReasonCodeText()).isEqualTo(DEFAULT_TAX_EXEMPTION_REASON_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxRateTypeCode()).isEqualTo(DEFAULT_TAX_RATE_TYPE_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxRateTypeCodeText()).isEqualTo(DEFAULT_TAX_RATE_TYPE_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxTypeCode()).isEqualTo(DEFAULT_TAX_TYPE_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxTypeCodeText()).isEqualTo(DEFAULT_TAX_TYPE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialDeviantTaxClassificationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialDeviantTaxClassificationRepository.findAll().size();

        // Create the MaterialDeviantTaxClassification with an existing ID
        materialDeviantTaxClassification.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialDeviantTaxClassificationMockMvc.perform(post("/api/material-deviant-tax-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialDeviantTaxClassification)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialDeviantTaxClassification in the database
        List<MaterialDeviantTaxClassification> materialDeviantTaxClassificationList = materialDeviantTaxClassificationRepository.findAll();
        assertThat(materialDeviantTaxClassificationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialDeviantTaxClassifications() throws Exception {
        // Initialize the database
        materialDeviantTaxClassificationRepository.saveAndFlush(materialDeviantTaxClassification);

        // Get all the materialDeviantTaxClassificationList
        restMaterialDeviantTaxClassificationMockMvc.perform(get("/api/material-deviant-tax-classifications?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialDeviantTaxClassification.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryCode").value(hasItem(DEFAULT_COUNTRY_CODE.toString())))
            .andExpect(jsonPath("$.[*].countryCodeText").value(hasItem(DEFAULT_COUNTRY_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].regionCode").value(hasItem(DEFAULT_REGION_CODE.toString())))
            .andExpect(jsonPath("$.[*].regionCodeText").value(hasItem(DEFAULT_REGION_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].taxExemptionReasonCode").value(hasItem(DEFAULT_TAX_EXEMPTION_REASON_CODE.toString())))
            .andExpect(jsonPath("$.[*].taxExemptionReasonCodeText").value(hasItem(DEFAULT_TAX_EXEMPTION_REASON_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].taxRateTypeCode").value(hasItem(DEFAULT_TAX_RATE_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].taxRateTypeCodeText").value(hasItem(DEFAULT_TAX_RATE_TYPE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].taxTypeCode").value(hasItem(DEFAULT_TAX_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].taxTypeCodeText").value(hasItem(DEFAULT_TAX_TYPE_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialDeviantTaxClassification() throws Exception {
        // Initialize the database
        materialDeviantTaxClassificationRepository.saveAndFlush(materialDeviantTaxClassification);

        // Get the materialDeviantTaxClassification
        restMaterialDeviantTaxClassificationMockMvc.perform(get("/api/material-deviant-tax-classifications/{id}", materialDeviantTaxClassification.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialDeviantTaxClassification.getId().intValue()))
            .andExpect(jsonPath("$.countryCode").value(DEFAULT_COUNTRY_CODE.toString()))
            .andExpect(jsonPath("$.countryCodeText").value(DEFAULT_COUNTRY_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.regionCode").value(DEFAULT_REGION_CODE.toString()))
            .andExpect(jsonPath("$.regionCodeText").value(DEFAULT_REGION_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.taxExemptionReasonCode").value(DEFAULT_TAX_EXEMPTION_REASON_CODE.toString()))
            .andExpect(jsonPath("$.taxExemptionReasonCodeText").value(DEFAULT_TAX_EXEMPTION_REASON_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.taxRateTypeCode").value(DEFAULT_TAX_RATE_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.taxRateTypeCodeText").value(DEFAULT_TAX_RATE_TYPE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.taxTypeCode").value(DEFAULT_TAX_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.taxTypeCodeText").value(DEFAULT_TAX_TYPE_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialDeviantTaxClassification() throws Exception {
        // Get the materialDeviantTaxClassification
        restMaterialDeviantTaxClassificationMockMvc.perform(get("/api/material-deviant-tax-classifications/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialDeviantTaxClassification() throws Exception {
        // Initialize the database
        materialDeviantTaxClassificationService.save(materialDeviantTaxClassification);

        int databaseSizeBeforeUpdate = materialDeviantTaxClassificationRepository.findAll().size();

        // Update the materialDeviantTaxClassification
        MaterialDeviantTaxClassification updatedMaterialDeviantTaxClassification = materialDeviantTaxClassificationRepository.findById(materialDeviantTaxClassification.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialDeviantTaxClassification are not directly saved in db
        em.detach(updatedMaterialDeviantTaxClassification);
        updatedMaterialDeviantTaxClassification
            .countryCode(UPDATED_COUNTRY_CODE)
            .countryCodeText(UPDATED_COUNTRY_CODE_TEXT)
            .regionCode(UPDATED_REGION_CODE)
            .regionCodeText(UPDATED_REGION_CODE_TEXT)
            .taxExemptionReasonCode(UPDATED_TAX_EXEMPTION_REASON_CODE)
            .taxExemptionReasonCodeText(UPDATED_TAX_EXEMPTION_REASON_CODE_TEXT)
            .taxRateTypeCode(UPDATED_TAX_RATE_TYPE_CODE)
            .taxRateTypeCodeText(UPDATED_TAX_RATE_TYPE_CODE_TEXT)
            .taxTypeCode(UPDATED_TAX_TYPE_CODE)
            .taxTypeCodeText(UPDATED_TAX_TYPE_CODE_TEXT);

        restMaterialDeviantTaxClassificationMockMvc.perform(put("/api/material-deviant-tax-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialDeviantTaxClassification)))
            .andExpect(status().isOk());

        // Validate the MaterialDeviantTaxClassification in the database
        List<MaterialDeviantTaxClassification> materialDeviantTaxClassificationList = materialDeviantTaxClassificationRepository.findAll();
        assertThat(materialDeviantTaxClassificationList).hasSize(databaseSizeBeforeUpdate);
        MaterialDeviantTaxClassification testMaterialDeviantTaxClassification = materialDeviantTaxClassificationList.get(materialDeviantTaxClassificationList.size() - 1);
        assertThat(testMaterialDeviantTaxClassification.getCountryCode()).isEqualTo(UPDATED_COUNTRY_CODE);
        assertThat(testMaterialDeviantTaxClassification.getCountryCodeText()).isEqualTo(UPDATED_COUNTRY_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getRegionCode()).isEqualTo(UPDATED_REGION_CODE);
        assertThat(testMaterialDeviantTaxClassification.getRegionCodeText()).isEqualTo(UPDATED_REGION_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxExemptionReasonCode()).isEqualTo(UPDATED_TAX_EXEMPTION_REASON_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxExemptionReasonCodeText()).isEqualTo(UPDATED_TAX_EXEMPTION_REASON_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxRateTypeCode()).isEqualTo(UPDATED_TAX_RATE_TYPE_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxRateTypeCodeText()).isEqualTo(UPDATED_TAX_RATE_TYPE_CODE_TEXT);
        assertThat(testMaterialDeviantTaxClassification.getTaxTypeCode()).isEqualTo(UPDATED_TAX_TYPE_CODE);
        assertThat(testMaterialDeviantTaxClassification.getTaxTypeCodeText()).isEqualTo(UPDATED_TAX_TYPE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialDeviantTaxClassification() throws Exception {
        int databaseSizeBeforeUpdate = materialDeviantTaxClassificationRepository.findAll().size();

        // Create the MaterialDeviantTaxClassification

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialDeviantTaxClassificationMockMvc.perform(put("/api/material-deviant-tax-classifications")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialDeviantTaxClassification)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialDeviantTaxClassification in the database
        List<MaterialDeviantTaxClassification> materialDeviantTaxClassificationList = materialDeviantTaxClassificationRepository.findAll();
        assertThat(materialDeviantTaxClassificationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialDeviantTaxClassification() throws Exception {
        // Initialize the database
        materialDeviantTaxClassificationService.save(materialDeviantTaxClassification);

        int databaseSizeBeforeDelete = materialDeviantTaxClassificationRepository.findAll().size();

        // Delete the materialDeviantTaxClassification
        restMaterialDeviantTaxClassificationMockMvc.perform(delete("/api/material-deviant-tax-classifications/{id}", materialDeviantTaxClassification.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialDeviantTaxClassification> materialDeviantTaxClassificationList = materialDeviantTaxClassificationRepository.findAll();
        assertThat(materialDeviantTaxClassificationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialDeviantTaxClassification.class);
        MaterialDeviantTaxClassification materialDeviantTaxClassification1 = new MaterialDeviantTaxClassification();
        materialDeviantTaxClassification1.setId(1L);
        MaterialDeviantTaxClassification materialDeviantTaxClassification2 = new MaterialDeviantTaxClassification();
        materialDeviantTaxClassification2.setId(materialDeviantTaxClassification1.getId());
        assertThat(materialDeviantTaxClassification1).isEqualTo(materialDeviantTaxClassification2);
        materialDeviantTaxClassification2.setId(2L);
        assertThat(materialDeviantTaxClassification1).isNotEqualTo(materialDeviantTaxClassification2);
        materialDeviantTaxClassification1.setId(null);
        assertThat(materialDeviantTaxClassification1).isNotEqualTo(materialDeviantTaxClassification2);
    }
}
