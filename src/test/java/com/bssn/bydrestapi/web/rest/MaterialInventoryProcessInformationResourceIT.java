package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation;
import com.bssn.bydrestapi.repository.MaterialInventoryProcessInformationRepository;
import com.bssn.bydrestapi.service.MaterialInventoryProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialInventoryProcessInformationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialInventoryProcessInformationResourceIT {

    private static final String DEFAULT_SITE_ID = "AAAAAAAAAA";
    private static final String UPDATED_SITE_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialInventoryProcessInformationRepository materialInventoryProcessInformationRepository;

    @Autowired
    private MaterialInventoryProcessInformationService materialInventoryProcessInformationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialInventoryProcessInformationMockMvc;

    private MaterialInventoryProcessInformation materialInventoryProcessInformation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialInventoryProcessInformationResource materialInventoryProcessInformationResource = new MaterialInventoryProcessInformationResource(materialInventoryProcessInformationService);
        this.restMaterialInventoryProcessInformationMockMvc = MockMvcBuilders.standaloneSetup(materialInventoryProcessInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialInventoryProcessInformation createEntity(EntityManager em) {
        MaterialInventoryProcessInformation materialInventoryProcessInformation = new MaterialInventoryProcessInformation()
            .siteID(DEFAULT_SITE_ID)
            .lifeCycleStatusCode(DEFAULT_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
        return materialInventoryProcessInformation;
    }

    @BeforeEach
    public void initTest() {
        materialInventoryProcessInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialInventoryProcessInformation() throws Exception {
        int databaseSizeBeforeCreate = materialInventoryProcessInformationRepository.findAll().size();

        // Create the MaterialInventoryProcessInformation
        restMaterialInventoryProcessInformationMockMvc.perform(post("/api/material-inventory-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialInventoryProcessInformation)))
            .andExpect(status().isCreated());

        // Validate the MaterialInventoryProcessInformation in the database
        List<MaterialInventoryProcessInformation> materialInventoryProcessInformationList = materialInventoryProcessInformationRepository.findAll();
        assertThat(materialInventoryProcessInformationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialInventoryProcessInformation testMaterialInventoryProcessInformation = materialInventoryProcessInformationList.get(materialInventoryProcessInformationList.size() - 1);
        assertThat(testMaterialInventoryProcessInformation.getSiteID()).isEqualTo(DEFAULT_SITE_ID);
        assertThat(testMaterialInventoryProcessInformation.getLifeCycleStatusCode()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialInventoryProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialInventoryProcessInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialInventoryProcessInformationRepository.findAll().size();

        // Create the MaterialInventoryProcessInformation with an existing ID
        materialInventoryProcessInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialInventoryProcessInformationMockMvc.perform(post("/api/material-inventory-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialInventoryProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialInventoryProcessInformation in the database
        List<MaterialInventoryProcessInformation> materialInventoryProcessInformationList = materialInventoryProcessInformationRepository.findAll();
        assertThat(materialInventoryProcessInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialInventoryProcessInformations() throws Exception {
        // Initialize the database
        materialInventoryProcessInformationRepository.saveAndFlush(materialInventoryProcessInformation);

        // Get all the materialInventoryProcessInformationList
        restMaterialInventoryProcessInformationMockMvc.perform(get("/api/material-inventory-process-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialInventoryProcessInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].siteID").value(hasItem(DEFAULT_SITE_ID.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCode").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCodeText").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialInventoryProcessInformation() throws Exception {
        // Initialize the database
        materialInventoryProcessInformationRepository.saveAndFlush(materialInventoryProcessInformation);

        // Get the materialInventoryProcessInformation
        restMaterialInventoryProcessInformationMockMvc.perform(get("/api/material-inventory-process-informations/{id}", materialInventoryProcessInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialInventoryProcessInformation.getId().intValue()))
            .andExpect(jsonPath("$.siteID").value(DEFAULT_SITE_ID.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCode").value(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCodeText").value(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialInventoryProcessInformation() throws Exception {
        // Get the materialInventoryProcessInformation
        restMaterialInventoryProcessInformationMockMvc.perform(get("/api/material-inventory-process-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialInventoryProcessInformation() throws Exception {
        // Initialize the database
        materialInventoryProcessInformationService.save(materialInventoryProcessInformation);

        int databaseSizeBeforeUpdate = materialInventoryProcessInformationRepository.findAll().size();

        // Update the materialInventoryProcessInformation
        MaterialInventoryProcessInformation updatedMaterialInventoryProcessInformation = materialInventoryProcessInformationRepository.findById(materialInventoryProcessInformation.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialInventoryProcessInformation are not directly saved in db
        em.detach(updatedMaterialInventoryProcessInformation);
        updatedMaterialInventoryProcessInformation
            .siteID(UPDATED_SITE_ID)
            .lifeCycleStatusCode(UPDATED_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);

        restMaterialInventoryProcessInformationMockMvc.perform(put("/api/material-inventory-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialInventoryProcessInformation)))
            .andExpect(status().isOk());

        // Validate the MaterialInventoryProcessInformation in the database
        List<MaterialInventoryProcessInformation> materialInventoryProcessInformationList = materialInventoryProcessInformationRepository.findAll();
        assertThat(materialInventoryProcessInformationList).hasSize(databaseSizeBeforeUpdate);
        MaterialInventoryProcessInformation testMaterialInventoryProcessInformation = materialInventoryProcessInformationList.get(materialInventoryProcessInformationList.size() - 1);
        assertThat(testMaterialInventoryProcessInformation.getSiteID()).isEqualTo(UPDATED_SITE_ID);
        assertThat(testMaterialInventoryProcessInformation.getLifeCycleStatusCode()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialInventoryProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialInventoryProcessInformation() throws Exception {
        int databaseSizeBeforeUpdate = materialInventoryProcessInformationRepository.findAll().size();

        // Create the MaterialInventoryProcessInformation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialInventoryProcessInformationMockMvc.perform(put("/api/material-inventory-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialInventoryProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialInventoryProcessInformation in the database
        List<MaterialInventoryProcessInformation> materialInventoryProcessInformationList = materialInventoryProcessInformationRepository.findAll();
        assertThat(materialInventoryProcessInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialInventoryProcessInformation() throws Exception {
        // Initialize the database
        materialInventoryProcessInformationService.save(materialInventoryProcessInformation);

        int databaseSizeBeforeDelete = materialInventoryProcessInformationRepository.findAll().size();

        // Delete the materialInventoryProcessInformation
        restMaterialInventoryProcessInformationMockMvc.perform(delete("/api/material-inventory-process-informations/{id}", materialInventoryProcessInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialInventoryProcessInformation> materialInventoryProcessInformationList = materialInventoryProcessInformationRepository.findAll();
        assertThat(materialInventoryProcessInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialInventoryProcessInformation.class);
        MaterialInventoryProcessInformation materialInventoryProcessInformation1 = new MaterialInventoryProcessInformation();
        materialInventoryProcessInformation1.setId(1L);
        MaterialInventoryProcessInformation materialInventoryProcessInformation2 = new MaterialInventoryProcessInformation();
        materialInventoryProcessInformation2.setId(materialInventoryProcessInformation1.getId());
        assertThat(materialInventoryProcessInformation1).isEqualTo(materialInventoryProcessInformation2);
        materialInventoryProcessInformation2.setId(2L);
        assertThat(materialInventoryProcessInformation1).isNotEqualTo(materialInventoryProcessInformation2);
        materialInventoryProcessInformation1.setId(null);
        assertThat(materialInventoryProcessInformation1).isNotEqualTo(materialInventoryProcessInformation2);
    }
}
