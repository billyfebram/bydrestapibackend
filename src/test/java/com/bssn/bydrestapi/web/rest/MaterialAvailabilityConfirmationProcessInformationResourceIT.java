package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation;
import com.bssn.bydrestapi.repository.MaterialAvailabilityConfirmationProcessInformationRepository;
import com.bssn.bydrestapi.service.MaterialAvailabilityConfirmationProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialAvailabilityConfirmationProcessInformationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialAvailabilityConfirmationProcessInformationResourceIT {

    private static final String DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLY_PLANNING_AREA_ID = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLY_PLANNING_AREA_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialAvailabilityConfirmationProcessInformationRepository materialAvailabilityConfirmationProcessInformationRepository;

    @Autowired
    private MaterialAvailabilityConfirmationProcessInformationService materialAvailabilityConfirmationProcessInformationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialAvailabilityConfirmationProcessInformationMockMvc;

    private MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialAvailabilityConfirmationProcessInformationResource materialAvailabilityConfirmationProcessInformationResource = new MaterialAvailabilityConfirmationProcessInformationResource(materialAvailabilityConfirmationProcessInformationService);
        this.restMaterialAvailabilityConfirmationProcessInformationMockMvc = MockMvcBuilders.standaloneSetup(materialAvailabilityConfirmationProcessInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialAvailabilityConfirmationProcessInformation createEntity(EntityManager em) {
        MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation = new MaterialAvailabilityConfirmationProcessInformation()
            .availabilityConfirmationModeCode(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE)
            .availabilityConfirmationModeCodeText(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT)
            .supplyPlanningAreaID(DEFAULT_SUPPLY_PLANNING_AREA_ID)
            .lifeCycleStatusCode(DEFAULT_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
        return materialAvailabilityConfirmationProcessInformation;
    }

    @BeforeEach
    public void initTest() {
        materialAvailabilityConfirmationProcessInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        int databaseSizeBeforeCreate = materialAvailabilityConfirmationProcessInformationRepository.findAll().size();

        // Create the MaterialAvailabilityConfirmationProcessInformation
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(post("/api/material-availability-confirmation-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialAvailabilityConfirmationProcessInformation)))
            .andExpect(status().isCreated());

        // Validate the MaterialAvailabilityConfirmationProcessInformation in the database
        List<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformationList = materialAvailabilityConfirmationProcessInformationRepository.findAll();
        assertThat(materialAvailabilityConfirmationProcessInformationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialAvailabilityConfirmationProcessInformation testMaterialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformationList.get(materialAvailabilityConfirmationProcessInformationList.size() - 1);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getAvailabilityConfirmationModeCode()).isEqualTo(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getAvailabilityConfirmationModeCodeText()).isEqualTo(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getSupplyPlanningAreaID()).isEqualTo(DEFAULT_SUPPLY_PLANNING_AREA_ID);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getLifeCycleStatusCode()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialAvailabilityConfirmationProcessInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialAvailabilityConfirmationProcessInformationRepository.findAll().size();

        // Create the MaterialAvailabilityConfirmationProcessInformation with an existing ID
        materialAvailabilityConfirmationProcessInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(post("/api/material-availability-confirmation-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialAvailabilityConfirmationProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialAvailabilityConfirmationProcessInformation in the database
        List<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformationList = materialAvailabilityConfirmationProcessInformationRepository.findAll();
        assertThat(materialAvailabilityConfirmationProcessInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialAvailabilityConfirmationProcessInformations() throws Exception {
        // Initialize the database
        materialAvailabilityConfirmationProcessInformationRepository.saveAndFlush(materialAvailabilityConfirmationProcessInformation);

        // Get all the materialAvailabilityConfirmationProcessInformationList
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(get("/api/material-availability-confirmation-process-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialAvailabilityConfirmationProcessInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].availabilityConfirmationModeCode").value(hasItem(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE.toString())))
            .andExpect(jsonPath("$.[*].availabilityConfirmationModeCodeText").value(hasItem(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].supplyPlanningAreaID").value(hasItem(DEFAULT_SUPPLY_PLANNING_AREA_ID.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCode").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCodeText").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        // Initialize the database
        materialAvailabilityConfirmationProcessInformationRepository.saveAndFlush(materialAvailabilityConfirmationProcessInformation);

        // Get the materialAvailabilityConfirmationProcessInformation
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(get("/api/material-availability-confirmation-process-informations/{id}", materialAvailabilityConfirmationProcessInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialAvailabilityConfirmationProcessInformation.getId().intValue()))
            .andExpect(jsonPath("$.availabilityConfirmationModeCode").value(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE.toString()))
            .andExpect(jsonPath("$.availabilityConfirmationModeCodeText").value(DEFAULT_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.supplyPlanningAreaID").value(DEFAULT_SUPPLY_PLANNING_AREA_ID.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCode").value(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCodeText").value(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        // Get the materialAvailabilityConfirmationProcessInformation
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(get("/api/material-availability-confirmation-process-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        // Initialize the database
        materialAvailabilityConfirmationProcessInformationService.save(materialAvailabilityConfirmationProcessInformation);

        int databaseSizeBeforeUpdate = materialAvailabilityConfirmationProcessInformationRepository.findAll().size();

        // Update the materialAvailabilityConfirmationProcessInformation
        MaterialAvailabilityConfirmationProcessInformation updatedMaterialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformationRepository.findById(materialAvailabilityConfirmationProcessInformation.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialAvailabilityConfirmationProcessInformation are not directly saved in db
        em.detach(updatedMaterialAvailabilityConfirmationProcessInformation);
        updatedMaterialAvailabilityConfirmationProcessInformation
            .availabilityConfirmationModeCode(UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE)
            .availabilityConfirmationModeCodeText(UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT)
            .supplyPlanningAreaID(UPDATED_SUPPLY_PLANNING_AREA_ID)
            .lifeCycleStatusCode(UPDATED_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);

        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(put("/api/material-availability-confirmation-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialAvailabilityConfirmationProcessInformation)))
            .andExpect(status().isOk());

        // Validate the MaterialAvailabilityConfirmationProcessInformation in the database
        List<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformationList = materialAvailabilityConfirmationProcessInformationRepository.findAll();
        assertThat(materialAvailabilityConfirmationProcessInformationList).hasSize(databaseSizeBeforeUpdate);
        MaterialAvailabilityConfirmationProcessInformation testMaterialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformationList.get(materialAvailabilityConfirmationProcessInformationList.size() - 1);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getAvailabilityConfirmationModeCode()).isEqualTo(UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getAvailabilityConfirmationModeCodeText()).isEqualTo(UPDATED_AVAILABILITY_CONFIRMATION_MODE_CODE_TEXT);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getSupplyPlanningAreaID()).isEqualTo(UPDATED_SUPPLY_PLANNING_AREA_ID);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getLifeCycleStatusCode()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialAvailabilityConfirmationProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        int databaseSizeBeforeUpdate = materialAvailabilityConfirmationProcessInformationRepository.findAll().size();

        // Create the MaterialAvailabilityConfirmationProcessInformation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(put("/api/material-availability-confirmation-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialAvailabilityConfirmationProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialAvailabilityConfirmationProcessInformation in the database
        List<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformationList = materialAvailabilityConfirmationProcessInformationRepository.findAll();
        assertThat(materialAvailabilityConfirmationProcessInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialAvailabilityConfirmationProcessInformation() throws Exception {
        // Initialize the database
        materialAvailabilityConfirmationProcessInformationService.save(materialAvailabilityConfirmationProcessInformation);

        int databaseSizeBeforeDelete = materialAvailabilityConfirmationProcessInformationRepository.findAll().size();

        // Delete the materialAvailabilityConfirmationProcessInformation
        restMaterialAvailabilityConfirmationProcessInformationMockMvc.perform(delete("/api/material-availability-confirmation-process-informations/{id}", materialAvailabilityConfirmationProcessInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformationList = materialAvailabilityConfirmationProcessInformationRepository.findAll();
        assertThat(materialAvailabilityConfirmationProcessInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialAvailabilityConfirmationProcessInformation.class);
        MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation1 = new MaterialAvailabilityConfirmationProcessInformation();
        materialAvailabilityConfirmationProcessInformation1.setId(1L);
        MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation2 = new MaterialAvailabilityConfirmationProcessInformation();
        materialAvailabilityConfirmationProcessInformation2.setId(materialAvailabilityConfirmationProcessInformation1.getId());
        assertThat(materialAvailabilityConfirmationProcessInformation1).isEqualTo(materialAvailabilityConfirmationProcessInformation2);
        materialAvailabilityConfirmationProcessInformation2.setId(2L);
        assertThat(materialAvailabilityConfirmationProcessInformation1).isNotEqualTo(materialAvailabilityConfirmationProcessInformation2);
        materialAvailabilityConfirmationProcessInformation1.setId(null);
        assertThat(materialAvailabilityConfirmationProcessInformation1).isNotEqualTo(materialAvailabilityConfirmationProcessInformation2);
    }
}
