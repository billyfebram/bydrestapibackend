package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialCrossProcessCategory;
import com.bssn.bydrestapi.repository.MaterialCrossProcessCategoryRepository;
import com.bssn.bydrestapi.service.MaterialCrossProcessCategoryService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialCrossProcessCategoryResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialCrossProcessCategoryResourceIT {

    private static final String DEFAULT_PRODUCT_CATEGORY_INTERNAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_CATEGORY_INTERNAL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_LANGUAGE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_LANGUAGE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_LANGUAGE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_LANGUAGE_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialCrossProcessCategoryRepository materialCrossProcessCategoryRepository;

    @Autowired
    private MaterialCrossProcessCategoryService materialCrossProcessCategoryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialCrossProcessCategoryMockMvc;

    private MaterialCrossProcessCategory materialCrossProcessCategory;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialCrossProcessCategoryResource materialCrossProcessCategoryResource = new MaterialCrossProcessCategoryResource(materialCrossProcessCategoryService);
        this.restMaterialCrossProcessCategoryMockMvc = MockMvcBuilders.standaloneSetup(materialCrossProcessCategoryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialCrossProcessCategory createEntity(EntityManager em) {
        MaterialCrossProcessCategory materialCrossProcessCategory = new MaterialCrossProcessCategory()
            .productCategoryInternalID(DEFAULT_PRODUCT_CATEGORY_INTERNAL_ID)
            .description(DEFAULT_DESCRIPTION)
            .descriptionLanguageCode(DEFAULT_DESCRIPTION_LANGUAGE_CODE)
            .descriptionLanguageCodeText(DEFAULT_DESCRIPTION_LANGUAGE_CODE_TEXT);
        return materialCrossProcessCategory;
    }

    @BeforeEach
    public void initTest() {
        materialCrossProcessCategory = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialCrossProcessCategory() throws Exception {
        int databaseSizeBeforeCreate = materialCrossProcessCategoryRepository.findAll().size();

        // Create the MaterialCrossProcessCategory
        restMaterialCrossProcessCategoryMockMvc.perform(post("/api/material-cross-process-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialCrossProcessCategory)))
            .andExpect(status().isCreated());

        // Validate the MaterialCrossProcessCategory in the database
        List<MaterialCrossProcessCategory> materialCrossProcessCategoryList = materialCrossProcessCategoryRepository.findAll();
        assertThat(materialCrossProcessCategoryList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialCrossProcessCategory testMaterialCrossProcessCategory = materialCrossProcessCategoryList.get(materialCrossProcessCategoryList.size() - 1);
        assertThat(testMaterialCrossProcessCategory.getProductCategoryInternalID()).isEqualTo(DEFAULT_PRODUCT_CATEGORY_INTERNAL_ID);
        assertThat(testMaterialCrossProcessCategory.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testMaterialCrossProcessCategory.getDescriptionLanguageCode()).isEqualTo(DEFAULT_DESCRIPTION_LANGUAGE_CODE);
        assertThat(testMaterialCrossProcessCategory.getDescriptionLanguageCodeText()).isEqualTo(DEFAULT_DESCRIPTION_LANGUAGE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialCrossProcessCategoryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialCrossProcessCategoryRepository.findAll().size();

        // Create the MaterialCrossProcessCategory with an existing ID
        materialCrossProcessCategory.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialCrossProcessCategoryMockMvc.perform(post("/api/material-cross-process-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialCrossProcessCategory)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialCrossProcessCategory in the database
        List<MaterialCrossProcessCategory> materialCrossProcessCategoryList = materialCrossProcessCategoryRepository.findAll();
        assertThat(materialCrossProcessCategoryList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialCrossProcessCategories() throws Exception {
        // Initialize the database
        materialCrossProcessCategoryRepository.saveAndFlush(materialCrossProcessCategory);

        // Get all the materialCrossProcessCategoryList
        restMaterialCrossProcessCategoryMockMvc.perform(get("/api/material-cross-process-categories?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialCrossProcessCategory.getId().intValue())))
            .andExpect(jsonPath("$.[*].productCategoryInternalID").value(hasItem(DEFAULT_PRODUCT_CATEGORY_INTERNAL_ID.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].descriptionLanguageCode").value(hasItem(DEFAULT_DESCRIPTION_LANGUAGE_CODE.toString())))
            .andExpect(jsonPath("$.[*].descriptionLanguageCodeText").value(hasItem(DEFAULT_DESCRIPTION_LANGUAGE_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialCrossProcessCategory() throws Exception {
        // Initialize the database
        materialCrossProcessCategoryRepository.saveAndFlush(materialCrossProcessCategory);

        // Get the materialCrossProcessCategory
        restMaterialCrossProcessCategoryMockMvc.perform(get("/api/material-cross-process-categories/{id}", materialCrossProcessCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialCrossProcessCategory.getId().intValue()))
            .andExpect(jsonPath("$.productCategoryInternalID").value(DEFAULT_PRODUCT_CATEGORY_INTERNAL_ID.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.descriptionLanguageCode").value(DEFAULT_DESCRIPTION_LANGUAGE_CODE.toString()))
            .andExpect(jsonPath("$.descriptionLanguageCodeText").value(DEFAULT_DESCRIPTION_LANGUAGE_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialCrossProcessCategory() throws Exception {
        // Get the materialCrossProcessCategory
        restMaterialCrossProcessCategoryMockMvc.perform(get("/api/material-cross-process-categories/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialCrossProcessCategory() throws Exception {
        // Initialize the database
        materialCrossProcessCategoryService.save(materialCrossProcessCategory);

        int databaseSizeBeforeUpdate = materialCrossProcessCategoryRepository.findAll().size();

        // Update the materialCrossProcessCategory
        MaterialCrossProcessCategory updatedMaterialCrossProcessCategory = materialCrossProcessCategoryRepository.findById(materialCrossProcessCategory.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialCrossProcessCategory are not directly saved in db
        em.detach(updatedMaterialCrossProcessCategory);
        updatedMaterialCrossProcessCategory
            .productCategoryInternalID(UPDATED_PRODUCT_CATEGORY_INTERNAL_ID)
            .description(UPDATED_DESCRIPTION)
            .descriptionLanguageCode(UPDATED_DESCRIPTION_LANGUAGE_CODE)
            .descriptionLanguageCodeText(UPDATED_DESCRIPTION_LANGUAGE_CODE_TEXT);

        restMaterialCrossProcessCategoryMockMvc.perform(put("/api/material-cross-process-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialCrossProcessCategory)))
            .andExpect(status().isOk());

        // Validate the MaterialCrossProcessCategory in the database
        List<MaterialCrossProcessCategory> materialCrossProcessCategoryList = materialCrossProcessCategoryRepository.findAll();
        assertThat(materialCrossProcessCategoryList).hasSize(databaseSizeBeforeUpdate);
        MaterialCrossProcessCategory testMaterialCrossProcessCategory = materialCrossProcessCategoryList.get(materialCrossProcessCategoryList.size() - 1);
        assertThat(testMaterialCrossProcessCategory.getProductCategoryInternalID()).isEqualTo(UPDATED_PRODUCT_CATEGORY_INTERNAL_ID);
        assertThat(testMaterialCrossProcessCategory.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMaterialCrossProcessCategory.getDescriptionLanguageCode()).isEqualTo(UPDATED_DESCRIPTION_LANGUAGE_CODE);
        assertThat(testMaterialCrossProcessCategory.getDescriptionLanguageCodeText()).isEqualTo(UPDATED_DESCRIPTION_LANGUAGE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialCrossProcessCategory() throws Exception {
        int databaseSizeBeforeUpdate = materialCrossProcessCategoryRepository.findAll().size();

        // Create the MaterialCrossProcessCategory

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialCrossProcessCategoryMockMvc.perform(put("/api/material-cross-process-categories")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialCrossProcessCategory)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialCrossProcessCategory in the database
        List<MaterialCrossProcessCategory> materialCrossProcessCategoryList = materialCrossProcessCategoryRepository.findAll();
        assertThat(materialCrossProcessCategoryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialCrossProcessCategory() throws Exception {
        // Initialize the database
        materialCrossProcessCategoryService.save(materialCrossProcessCategory);

        int databaseSizeBeforeDelete = materialCrossProcessCategoryRepository.findAll().size();

        // Delete the materialCrossProcessCategory
        restMaterialCrossProcessCategoryMockMvc.perform(delete("/api/material-cross-process-categories/{id}", materialCrossProcessCategory.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialCrossProcessCategory> materialCrossProcessCategoryList = materialCrossProcessCategoryRepository.findAll();
        assertThat(materialCrossProcessCategoryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialCrossProcessCategory.class);
        MaterialCrossProcessCategory materialCrossProcessCategory1 = new MaterialCrossProcessCategory();
        materialCrossProcessCategory1.setId(1L);
        MaterialCrossProcessCategory materialCrossProcessCategory2 = new MaterialCrossProcessCategory();
        materialCrossProcessCategory2.setId(materialCrossProcessCategory1.getId());
        assertThat(materialCrossProcessCategory1).isEqualTo(materialCrossProcessCategory2);
        materialCrossProcessCategory2.setId(2L);
        assertThat(materialCrossProcessCategory1).isNotEqualTo(materialCrossProcessCategory2);
        materialCrossProcessCategory1.setId(null);
        assertThat(materialCrossProcessCategory1).isNotEqualTo(materialCrossProcessCategory2);
    }
}
