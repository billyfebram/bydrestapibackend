package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.Material;
import com.bssn.bydrestapi.repository.MaterialRepository;
import com.bssn.bydrestapi.service.MaterialService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialResourceIT {

    private static final String DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PROCUREMENT_MEASURE_UNIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_PROCUREMENT_MEASURE_UNIT_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_INTERNAL_ID = "AAAAAAAAAA";
    private static final String UPDATED_INTERNAL_ID = "BBBBBBBBBB";

    private static final String DEFAULT_U_UID = "AAAAAAAAAA";
    private static final String UPDATED_U_UID = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_MEASURE_UNIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_BASE_MEASURE_UNIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_BASE_MEASURE_UNIT_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_BASE_MEASURE_UNIT_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFIED_STOCK_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIED_STOCK_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_IDENTIFIED_STOCK_TYPE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_IDENTIFIED_STOCK_TYPE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_LANGUAGE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LANGUAGE_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialRepository materialRepository;

    @Mock
    private MaterialRepository materialRepositoryMock;

    @Mock
    private MaterialService materialServiceMock;

    @Autowired
    private MaterialService materialService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialMockMvc;

    private Material material;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialResource materialResource = new MaterialResource(materialService);
        this.restMaterialMockMvc = MockMvcBuilders.standaloneSetup(materialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Material createEntity(EntityManager em) {
        Material material = new Material()
            .procurementMeasureUnitCode(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE)
            .procurementMeasureUnitCodeText(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE_TEXT)
            .procurementLifeCycleStatusCode(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE)
            .procurementLifeCycleStatusCodeText(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT)
            .internalID(DEFAULT_INTERNAL_ID)
            .uUID(DEFAULT_U_UID)
            .baseMeasureUnitCode(DEFAULT_BASE_MEASURE_UNIT_CODE)
            .baseMeasureUnitCodeText(DEFAULT_BASE_MEASURE_UNIT_CODE_TEXT)
            .identifiedStockTypeCode(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE)
            .identifiedStockTypeCodeText(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE_TEXT)
            .languageCode(DEFAULT_LANGUAGE_CODE)
            .description(DEFAULT_DESCRIPTION)
            .languageCodeText(DEFAULT_LANGUAGE_CODE_TEXT)
            .productValuationLevelTypeCode(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE)
            .productValuationLevelTypeCodeText(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT);
        return material;
    }

    @BeforeEach
    public void initTest() {
        material = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterial() throws Exception {
        int databaseSizeBeforeCreate = materialRepository.findAll().size();

        // Create the Material
        restMaterialMockMvc.perform(post("/api/materials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(material)))
            .andExpect(status().isCreated());

        // Validate the Material in the database
        List<Material> materialList = materialRepository.findAll();
        assertThat(materialList).hasSize(databaseSizeBeforeCreate + 1);
        Material testMaterial = materialList.get(materialList.size() - 1);
        assertThat(testMaterial.getProcurementMeasureUnitCode()).isEqualTo(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE);
        assertThat(testMaterial.getProcurementMeasureUnitCodeText()).isEqualTo(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterial.getProcurementLifeCycleStatusCode()).isEqualTo(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterial.getProcurementLifeCycleStatusCodeText()).isEqualTo(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT);
        assertThat(testMaterial.getInternalID()).isEqualTo(DEFAULT_INTERNAL_ID);
        assertThat(testMaterial.getuUID()).isEqualTo(DEFAULT_U_UID);
        assertThat(testMaterial.getBaseMeasureUnitCode()).isEqualTo(DEFAULT_BASE_MEASURE_UNIT_CODE);
        assertThat(testMaterial.getBaseMeasureUnitCodeText()).isEqualTo(DEFAULT_BASE_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterial.getIdentifiedStockTypeCode()).isEqualTo(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE);
        assertThat(testMaterial.getIdentifiedStockTypeCodeText()).isEqualTo(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE_TEXT);
        assertThat(testMaterial.getLanguageCode()).isEqualTo(DEFAULT_LANGUAGE_CODE);
        assertThat(testMaterial.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testMaterial.getLanguageCodeText()).isEqualTo(DEFAULT_LANGUAGE_CODE_TEXT);
        assertThat(testMaterial.getProductValuationLevelTypeCode()).isEqualTo(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE);
        assertThat(testMaterial.getProductValuationLevelTypeCodeText()).isEqualTo(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialRepository.findAll().size();

        // Create the Material with an existing ID
        material.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialMockMvc.perform(post("/api/materials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(material)))
            .andExpect(status().isBadRequest());

        // Validate the Material in the database
        List<Material> materialList = materialRepository.findAll();
        assertThat(materialList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterials() throws Exception {
        // Initialize the database
        materialRepository.saveAndFlush(material);

        // Get all the materialList
        restMaterialMockMvc.perform(get("/api/materials?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(material.getId().intValue())))
            .andExpect(jsonPath("$.[*].procurementMeasureUnitCode").value(hasItem(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE.toString())))
            .andExpect(jsonPath("$.[*].procurementMeasureUnitCodeText").value(hasItem(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].procurementLifeCycleStatusCode").value(hasItem(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].procurementLifeCycleStatusCodeText").value(hasItem(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].internalID").value(hasItem(DEFAULT_INTERNAL_ID.toString())))
            .andExpect(jsonPath("$.[*].uUID").value(hasItem(DEFAULT_U_UID.toString())))
            .andExpect(jsonPath("$.[*].baseMeasureUnitCode").value(hasItem(DEFAULT_BASE_MEASURE_UNIT_CODE.toString())))
            .andExpect(jsonPath("$.[*].baseMeasureUnitCodeText").value(hasItem(DEFAULT_BASE_MEASURE_UNIT_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].identifiedStockTypeCode").value(hasItem(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].identifiedStockTypeCodeText").value(hasItem(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].languageCode").value(hasItem(DEFAULT_LANGUAGE_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].languageCodeText").value(hasItem(DEFAULT_LANGUAGE_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].productValuationLevelTypeCode").value(hasItem(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE.toString())))
            .andExpect(jsonPath("$.[*].productValuationLevelTypeCodeText").value(hasItem(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT.toString())));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllMaterialsWithEagerRelationshipsIsEnabled() throws Exception {
        MaterialResource materialResource = new MaterialResource(materialServiceMock);
        when(materialServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        MockMvc restMaterialMockMvc = MockMvcBuilders.standaloneSetup(materialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restMaterialMockMvc.perform(get("/api/materials?eagerload=true"))
        .andExpect(status().isOk());

        verify(materialServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllMaterialsWithEagerRelationshipsIsNotEnabled() throws Exception {
        MaterialResource materialResource = new MaterialResource(materialServiceMock);
            when(materialServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));
            MockMvc restMaterialMockMvc = MockMvcBuilders.standaloneSetup(materialResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();

        restMaterialMockMvc.perform(get("/api/materials?eagerload=true"))
        .andExpect(status().isOk());

            verify(materialServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    @Transactional
    public void getMaterial() throws Exception {
        // Initialize the database
        materialRepository.saveAndFlush(material);

        // Get the material
        restMaterialMockMvc.perform(get("/api/materials/{id}", material.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(material.getId().intValue()))
            .andExpect(jsonPath("$.procurementMeasureUnitCode").value(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE.toString()))
            .andExpect(jsonPath("$.procurementMeasureUnitCodeText").value(DEFAULT_PROCUREMENT_MEASURE_UNIT_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.procurementLifeCycleStatusCode").value(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.procurementLifeCycleStatusCodeText").value(DEFAULT_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.internalID").value(DEFAULT_INTERNAL_ID.toString()))
            .andExpect(jsonPath("$.uUID").value(DEFAULT_U_UID.toString()))
            .andExpect(jsonPath("$.baseMeasureUnitCode").value(DEFAULT_BASE_MEASURE_UNIT_CODE.toString()))
            .andExpect(jsonPath("$.baseMeasureUnitCodeText").value(DEFAULT_BASE_MEASURE_UNIT_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.identifiedStockTypeCode").value(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.identifiedStockTypeCodeText").value(DEFAULT_IDENTIFIED_STOCK_TYPE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.languageCode").value(DEFAULT_LANGUAGE_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.languageCodeText").value(DEFAULT_LANGUAGE_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.productValuationLevelTypeCode").value(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE.toString()))
            .andExpect(jsonPath("$.productValuationLevelTypeCodeText").value(DEFAULT_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterial() throws Exception {
        // Get the material
        restMaterialMockMvc.perform(get("/api/materials/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterial() throws Exception {
        // Initialize the database
        materialService.save(material);

        int databaseSizeBeforeUpdate = materialRepository.findAll().size();

        // Update the material
        Material updatedMaterial = materialRepository.findById(material.getId()).get();
        // Disconnect from session so that the updates on updatedMaterial are not directly saved in db
        em.detach(updatedMaterial);
        updatedMaterial
            .procurementMeasureUnitCode(UPDATED_PROCUREMENT_MEASURE_UNIT_CODE)
            .procurementMeasureUnitCodeText(UPDATED_PROCUREMENT_MEASURE_UNIT_CODE_TEXT)
            .procurementLifeCycleStatusCode(UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE)
            .procurementLifeCycleStatusCodeText(UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT)
            .internalID(UPDATED_INTERNAL_ID)
            .uUID(UPDATED_U_UID)
            .baseMeasureUnitCode(UPDATED_BASE_MEASURE_UNIT_CODE)
            .baseMeasureUnitCodeText(UPDATED_BASE_MEASURE_UNIT_CODE_TEXT)
            .identifiedStockTypeCode(UPDATED_IDENTIFIED_STOCK_TYPE_CODE)
            .identifiedStockTypeCodeText(UPDATED_IDENTIFIED_STOCK_TYPE_CODE_TEXT)
            .languageCode(UPDATED_LANGUAGE_CODE)
            .description(UPDATED_DESCRIPTION)
            .languageCodeText(UPDATED_LANGUAGE_CODE_TEXT)
            .productValuationLevelTypeCode(UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE)
            .productValuationLevelTypeCodeText(UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT);

        restMaterialMockMvc.perform(put("/api/materials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterial)))
            .andExpect(status().isOk());

        // Validate the Material in the database
        List<Material> materialList = materialRepository.findAll();
        assertThat(materialList).hasSize(databaseSizeBeforeUpdate);
        Material testMaterial = materialList.get(materialList.size() - 1);
        assertThat(testMaterial.getProcurementMeasureUnitCode()).isEqualTo(UPDATED_PROCUREMENT_MEASURE_UNIT_CODE);
        assertThat(testMaterial.getProcurementMeasureUnitCodeText()).isEqualTo(UPDATED_PROCUREMENT_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterial.getProcurementLifeCycleStatusCode()).isEqualTo(UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterial.getProcurementLifeCycleStatusCodeText()).isEqualTo(UPDATED_PROCUREMENT_LIFE_CYCLE_STATUS_CODE_TEXT);
        assertThat(testMaterial.getInternalID()).isEqualTo(UPDATED_INTERNAL_ID);
        assertThat(testMaterial.getuUID()).isEqualTo(UPDATED_U_UID);
        assertThat(testMaterial.getBaseMeasureUnitCode()).isEqualTo(UPDATED_BASE_MEASURE_UNIT_CODE);
        assertThat(testMaterial.getBaseMeasureUnitCodeText()).isEqualTo(UPDATED_BASE_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterial.getIdentifiedStockTypeCode()).isEqualTo(UPDATED_IDENTIFIED_STOCK_TYPE_CODE);
        assertThat(testMaterial.getIdentifiedStockTypeCodeText()).isEqualTo(UPDATED_IDENTIFIED_STOCK_TYPE_CODE_TEXT);
        assertThat(testMaterial.getLanguageCode()).isEqualTo(UPDATED_LANGUAGE_CODE);
        assertThat(testMaterial.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMaterial.getLanguageCodeText()).isEqualTo(UPDATED_LANGUAGE_CODE_TEXT);
        assertThat(testMaterial.getProductValuationLevelTypeCode()).isEqualTo(UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE);
        assertThat(testMaterial.getProductValuationLevelTypeCodeText()).isEqualTo(UPDATED_PRODUCT_VALUATION_LEVEL_TYPE_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterial() throws Exception {
        int databaseSizeBeforeUpdate = materialRepository.findAll().size();

        // Create the Material

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialMockMvc.perform(put("/api/materials")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(material)))
            .andExpect(status().isBadRequest());

        // Validate the Material in the database
        List<Material> materialList = materialRepository.findAll();
        assertThat(materialList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterial() throws Exception {
        // Initialize the database
        materialService.save(material);

        int databaseSizeBeforeDelete = materialRepository.findAll().size();

        // Delete the material
        restMaterialMockMvc.perform(delete("/api/materials/{id}", material.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<Material> materialList = materialRepository.findAll();
        assertThat(materialList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Material.class);
        Material material1 = new Material();
        material1.setId(1L);
        Material material2 = new Material();
        material2.setId(material1.getId());
        assertThat(material1).isEqualTo(material2);
        material2.setId(2L);
        assertThat(material1).isNotEqualTo(material2);
        material1.setId(null);
        assertThat(material1).isNotEqualTo(material2);
    }
}
