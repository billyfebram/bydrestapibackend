package com.bssn.bydrestapi.web.rest;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;
import com.bssn.bydrestapi.repository.MaterialSalesProcessInformationRepository;
import com.bssn.bydrestapi.service.MaterialSalesProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

/**
 * Integration tests for the {@Link MaterialSalesProcessInformationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialSalesProcessInformationResourceIT {

    private static final String DEFAULT_SALES_ORGANISATION_ID = "AAAAAAAAAA";
    private static final String UPDATED_SALES_ORGANISATION_ID = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRIBUTION_CHANNEL_CODE = "AAAAAAAAAA";
    private static final String UPDATED_DISTRIBUTION_CHANNEL_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DISTRIBUTION_CHANNEL_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_DISTRIBUTION_CHANNEL_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SALES_MEASURE_UNIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_SALES_MEASURE_UNIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_SALES_MEASURE_UNIT_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_SALES_MEASURE_UNIT_CODE_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT = "AAAAAAAAAA";
    private static final String UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR = false;
    private static final Boolean UPDATED_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR = true;

    private static final BigDecimal DEFAULT_MINIMUM_ORDER_QUANTITY = new BigDecimal(1);
    private static final BigDecimal UPDATED_MINIMUM_ORDER_QUANTITY = new BigDecimal(2);

    @Autowired
    private MaterialSalesProcessInformationRepository materialSalesProcessInformationRepository;

    @Autowired
    private MaterialSalesProcessInformationService materialSalesProcessInformationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialSalesProcessInformationMockMvc;

    private MaterialSalesProcessInformation materialSalesProcessInformation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialSalesProcessInformationResource materialSalesProcessInformationResource = new MaterialSalesProcessInformationResource(materialSalesProcessInformationService);
        this.restMaterialSalesProcessInformationMockMvc = MockMvcBuilders.standaloneSetup(materialSalesProcessInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialSalesProcessInformation createEntity(EntityManager em) {
        MaterialSalesProcessInformation materialSalesProcessInformation = new MaterialSalesProcessInformation()
            .salesOrganisationID(DEFAULT_SALES_ORGANISATION_ID)
            .distributionChannelCode(DEFAULT_DISTRIBUTION_CHANNEL_CODE)
            .distributionChannelCodeText(DEFAULT_DISTRIBUTION_CHANNEL_CODE_TEXT)
            .lifeCycleStatusCode(DEFAULT_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT)
            .salesMeasureUnitCode(DEFAULT_SALES_MEASURE_UNIT_CODE)
            .salesMeasureUnitCodeText(DEFAULT_SALES_MEASURE_UNIT_CODE_TEXT)
            .customerTrxDocItemProcessingTypeDeterminationProductGroupCode(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE)
            .customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT)
            .cashDiscountDeductibleIndicator(DEFAULT_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR)
            .minimumOrderQuantity(DEFAULT_MINIMUM_ORDER_QUANTITY);
        return materialSalesProcessInformation;
    }

    @BeforeEach
    public void initTest() {
        materialSalesProcessInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialSalesProcessInformation() throws Exception {
        int databaseSizeBeforeCreate = materialSalesProcessInformationRepository.findAll().size();

        // Create the MaterialSalesProcessInformation
        restMaterialSalesProcessInformationMockMvc.perform(post("/api/material-sales-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSalesProcessInformation)))
            .andExpect(status().isCreated());

        // Validate the MaterialSalesProcessInformation in the database
        List<MaterialSalesProcessInformation> materialSalesProcessInformationList = materialSalesProcessInformationRepository.findAll();
        assertThat(materialSalesProcessInformationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialSalesProcessInformation testMaterialSalesProcessInformation = materialSalesProcessInformationList.get(materialSalesProcessInformationList.size() - 1);
        assertThat(testMaterialSalesProcessInformation.getSalesOrganisationID()).isEqualTo(DEFAULT_SALES_ORGANISATION_ID);
        assertThat(testMaterialSalesProcessInformation.getDistributionChannelCode()).isEqualTo(DEFAULT_DISTRIBUTION_CHANNEL_CODE);
        assertThat(testMaterialSalesProcessInformation.getDistributionChannelCodeText()).isEqualTo(DEFAULT_DISTRIBUTION_CHANNEL_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getLifeCycleStatusCode()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialSalesProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getSalesMeasureUnitCode()).isEqualTo(DEFAULT_SALES_MEASURE_UNIT_CODE);
        assertThat(testMaterialSalesProcessInformation.getSalesMeasureUnitCodeText()).isEqualTo(DEFAULT_SALES_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode()).isEqualTo(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE);
        assertThat(testMaterialSalesProcessInformation.getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt()).isEqualTo(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT);
        assertThat(testMaterialSalesProcessInformation.isCashDiscountDeductibleIndicator()).isEqualTo(DEFAULT_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR);
        assertThat(testMaterialSalesProcessInformation.getMinimumOrderQuantity()).isEqualTo(DEFAULT_MINIMUM_ORDER_QUANTITY);
    }

    @Test
    @Transactional
    public void createMaterialSalesProcessInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialSalesProcessInformationRepository.findAll().size();

        // Create the MaterialSalesProcessInformation with an existing ID
        materialSalesProcessInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialSalesProcessInformationMockMvc.perform(post("/api/material-sales-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSalesProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialSalesProcessInformation in the database
        List<MaterialSalesProcessInformation> materialSalesProcessInformationList = materialSalesProcessInformationRepository.findAll();
        assertThat(materialSalesProcessInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialSalesProcessInformations() throws Exception {
        // Initialize the database
        materialSalesProcessInformationRepository.saveAndFlush(materialSalesProcessInformation);

        // Get all the materialSalesProcessInformationList
        restMaterialSalesProcessInformationMockMvc.perform(get("/api/material-sales-process-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialSalesProcessInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].salesOrganisationID").value(hasItem(DEFAULT_SALES_ORGANISATION_ID.toString())))
            .andExpect(jsonPath("$.[*].distributionChannelCode").value(hasItem(DEFAULT_DISTRIBUTION_CHANNEL_CODE.toString())))
            .andExpect(jsonPath("$.[*].distributionChannelCodeText").value(hasItem(DEFAULT_DISTRIBUTION_CHANNEL_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCode").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCodeText").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].salesMeasureUnitCode").value(hasItem(DEFAULT_SALES_MEASURE_UNIT_CODE.toString())))
            .andExpect(jsonPath("$.[*].salesMeasureUnitCodeText").value(hasItem(DEFAULT_SALES_MEASURE_UNIT_CODE_TEXT.toString())))
            .andExpect(jsonPath("$.[*].customerTrxDocItemProcessingTypeDeterminationProductGroupCode").value(hasItem(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE.toString())))
            .andExpect(jsonPath("$.[*].customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt").value(hasItem(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT.toString())))
            .andExpect(jsonPath("$.[*].cashDiscountDeductibleIndicator").value(hasItem(DEFAULT_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR.booleanValue())))
            .andExpect(jsonPath("$.[*].minimumOrderQuantity").value(hasItem(DEFAULT_MINIMUM_ORDER_QUANTITY.doubleValue())));
    }
    
    @Test
    @Transactional
    public void getMaterialSalesProcessInformation() throws Exception {
        // Initialize the database
        materialSalesProcessInformationRepository.saveAndFlush(materialSalesProcessInformation);

        // Get the materialSalesProcessInformation
        restMaterialSalesProcessInformationMockMvc.perform(get("/api/material-sales-process-informations/{id}", materialSalesProcessInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialSalesProcessInformation.getId().intValue()))
            .andExpect(jsonPath("$.salesOrganisationID").value(DEFAULT_SALES_ORGANISATION_ID.toString()))
            .andExpect(jsonPath("$.distributionChannelCode").value(DEFAULT_DISTRIBUTION_CHANNEL_CODE.toString()))
            .andExpect(jsonPath("$.distributionChannelCodeText").value(DEFAULT_DISTRIBUTION_CHANNEL_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCode").value(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCodeText").value(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.salesMeasureUnitCode").value(DEFAULT_SALES_MEASURE_UNIT_CODE.toString()))
            .andExpect(jsonPath("$.salesMeasureUnitCodeText").value(DEFAULT_SALES_MEASURE_UNIT_CODE_TEXT.toString()))
            .andExpect(jsonPath("$.customerTrxDocItemProcessingTypeDeterminationProductGroupCode").value(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE.toString()))
            .andExpect(jsonPath("$.customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt").value(DEFAULT_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT.toString()))
            .andExpect(jsonPath("$.cashDiscountDeductibleIndicator").value(DEFAULT_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR.booleanValue()))
            .andExpect(jsonPath("$.minimumOrderQuantity").value(DEFAULT_MINIMUM_ORDER_QUANTITY.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialSalesProcessInformation() throws Exception {
        // Get the materialSalesProcessInformation
        restMaterialSalesProcessInformationMockMvc.perform(get("/api/material-sales-process-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialSalesProcessInformation() throws Exception {
        // Initialize the database
        materialSalesProcessInformationService.save(materialSalesProcessInformation);

        int databaseSizeBeforeUpdate = materialSalesProcessInformationRepository.findAll().size();

        // Update the materialSalesProcessInformation
        MaterialSalesProcessInformation updatedMaterialSalesProcessInformation = materialSalesProcessInformationRepository.findById(materialSalesProcessInformation.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialSalesProcessInformation are not directly saved in db
        em.detach(updatedMaterialSalesProcessInformation);
        updatedMaterialSalesProcessInformation
            .salesOrganisationID(UPDATED_SALES_ORGANISATION_ID)
            .distributionChannelCode(UPDATED_DISTRIBUTION_CHANNEL_CODE)
            .distributionChannelCodeText(UPDATED_DISTRIBUTION_CHANNEL_CODE_TEXT)
            .lifeCycleStatusCode(UPDATED_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT)
            .salesMeasureUnitCode(UPDATED_SALES_MEASURE_UNIT_CODE)
            .salesMeasureUnitCodeText(UPDATED_SALES_MEASURE_UNIT_CODE_TEXT)
            .customerTrxDocItemProcessingTypeDeterminationProductGroupCode(UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE)
            .customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt(UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT)
            .cashDiscountDeductibleIndicator(UPDATED_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR)
            .minimumOrderQuantity(UPDATED_MINIMUM_ORDER_QUANTITY);

        restMaterialSalesProcessInformationMockMvc.perform(put("/api/material-sales-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialSalesProcessInformation)))
            .andExpect(status().isOk());

        // Validate the MaterialSalesProcessInformation in the database
        List<MaterialSalesProcessInformation> materialSalesProcessInformationList = materialSalesProcessInformationRepository.findAll();
        assertThat(materialSalesProcessInformationList).hasSize(databaseSizeBeforeUpdate);
        MaterialSalesProcessInformation testMaterialSalesProcessInformation = materialSalesProcessInformationList.get(materialSalesProcessInformationList.size() - 1);
        assertThat(testMaterialSalesProcessInformation.getSalesOrganisationID()).isEqualTo(UPDATED_SALES_ORGANISATION_ID);
        assertThat(testMaterialSalesProcessInformation.getDistributionChannelCode()).isEqualTo(UPDATED_DISTRIBUTION_CHANNEL_CODE);
        assertThat(testMaterialSalesProcessInformation.getDistributionChannelCodeText()).isEqualTo(UPDATED_DISTRIBUTION_CHANNEL_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getLifeCycleStatusCode()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialSalesProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getSalesMeasureUnitCode()).isEqualTo(UPDATED_SALES_MEASURE_UNIT_CODE);
        assertThat(testMaterialSalesProcessInformation.getSalesMeasureUnitCodeText()).isEqualTo(UPDATED_SALES_MEASURE_UNIT_CODE_TEXT);
        assertThat(testMaterialSalesProcessInformation.getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode()).isEqualTo(UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE);
        assertThat(testMaterialSalesProcessInformation.getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt()).isEqualTo(UPDATED_CUSTOMER_TRX_DOC_ITEM_PROCESSING_TYPE_DETERMINATION_PRODUCT_GROUP_CODE_TXT);
        assertThat(testMaterialSalesProcessInformation.isCashDiscountDeductibleIndicator()).isEqualTo(UPDATED_CASH_DISCOUNT_DEDUCTIBLE_INDICATOR);
        assertThat(testMaterialSalesProcessInformation.getMinimumOrderQuantity()).isEqualTo(UPDATED_MINIMUM_ORDER_QUANTITY);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialSalesProcessInformation() throws Exception {
        int databaseSizeBeforeUpdate = materialSalesProcessInformationRepository.findAll().size();

        // Create the MaterialSalesProcessInformation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialSalesProcessInformationMockMvc.perform(put("/api/material-sales-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialSalesProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialSalesProcessInformation in the database
        List<MaterialSalesProcessInformation> materialSalesProcessInformationList = materialSalesProcessInformationRepository.findAll();
        assertThat(materialSalesProcessInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialSalesProcessInformation() throws Exception {
        // Initialize the database
        materialSalesProcessInformationService.save(materialSalesProcessInformation);

        int databaseSizeBeforeDelete = materialSalesProcessInformationRepository.findAll().size();

        // Delete the materialSalesProcessInformation
        restMaterialSalesProcessInformationMockMvc.perform(delete("/api/material-sales-process-informations/{id}", materialSalesProcessInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialSalesProcessInformation> materialSalesProcessInformationList = materialSalesProcessInformationRepository.findAll();
        assertThat(materialSalesProcessInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialSalesProcessInformation.class);
        MaterialSalesProcessInformation materialSalesProcessInformation1 = new MaterialSalesProcessInformation();
        materialSalesProcessInformation1.setId(1L);
        MaterialSalesProcessInformation materialSalesProcessInformation2 = new MaterialSalesProcessInformation();
        materialSalesProcessInformation2.setId(materialSalesProcessInformation1.getId());
        assertThat(materialSalesProcessInformation1).isEqualTo(materialSalesProcessInformation2);
        materialSalesProcessInformation2.setId(2L);
        assertThat(materialSalesProcessInformation1).isNotEqualTo(materialSalesProcessInformation2);
        materialSalesProcessInformation1.setId(null);
        assertThat(materialSalesProcessInformation1).isNotEqualTo(materialSalesProcessInformation2);
    }
}
