package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.BydRestApiBackendApp;
import com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation;
import com.bssn.bydrestapi.repository.MaterialFinancialProcessInformationRepository;
import com.bssn.bydrestapi.service.MaterialFinancialProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.bssn.bydrestapi.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@Link MaterialFinancialProcessInformationResource} REST controller.
 */
@SpringBootTest(classes = BydRestApiBackendApp.class)
public class MaterialFinancialProcessInformationResourceIT {

    private static final String DEFAULT_COMPANY_ID = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY_ID = "BBBBBBBBBB";

    private static final String DEFAULT_PERMANENT_ESTABLISHMENT_ID = "AAAAAAAAAA";
    private static final String UPDATED_PERMANENT_ESTABLISHMENT_ID = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT = "BBBBBBBBBB";

    @Autowired
    private MaterialFinancialProcessInformationRepository materialFinancialProcessInformationRepository;

    @Autowired
    private MaterialFinancialProcessInformationService materialFinancialProcessInformationService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMaterialFinancialProcessInformationMockMvc;

    private MaterialFinancialProcessInformation materialFinancialProcessInformation;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MaterialFinancialProcessInformationResource materialFinancialProcessInformationResource = new MaterialFinancialProcessInformationResource(materialFinancialProcessInformationService);
        this.restMaterialFinancialProcessInformationMockMvc = MockMvcBuilders.standaloneSetup(materialFinancialProcessInformationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MaterialFinancialProcessInformation createEntity(EntityManager em) {
        MaterialFinancialProcessInformation materialFinancialProcessInformation = new MaterialFinancialProcessInformation()
            .companyID(DEFAULT_COMPANY_ID)
            .permanentEstablishmentID(DEFAULT_PERMANENT_ESTABLISHMENT_ID)
            .lifeCycleStatusCode(DEFAULT_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
        return materialFinancialProcessInformation;
    }

    @BeforeEach
    public void initTest() {
        materialFinancialProcessInformation = createEntity(em);
    }

    @Test
    @Transactional
    public void createMaterialFinancialProcessInformation() throws Exception {
        int databaseSizeBeforeCreate = materialFinancialProcessInformationRepository.findAll().size();

        // Create the MaterialFinancialProcessInformation
        restMaterialFinancialProcessInformationMockMvc.perform(post("/api/material-financial-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialFinancialProcessInformation)))
            .andExpect(status().isCreated());

        // Validate the MaterialFinancialProcessInformation in the database
        List<MaterialFinancialProcessInformation> materialFinancialProcessInformationList = materialFinancialProcessInformationRepository.findAll();
        assertThat(materialFinancialProcessInformationList).hasSize(databaseSizeBeforeCreate + 1);
        MaterialFinancialProcessInformation testMaterialFinancialProcessInformation = materialFinancialProcessInformationList.get(materialFinancialProcessInformationList.size() - 1);
        assertThat(testMaterialFinancialProcessInformation.getCompanyID()).isEqualTo(DEFAULT_COMPANY_ID);
        assertThat(testMaterialFinancialProcessInformation.getPermanentEstablishmentID()).isEqualTo(DEFAULT_PERMANENT_ESTABLISHMENT_ID);
        assertThat(testMaterialFinancialProcessInformation.getLifeCycleStatusCode()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialFinancialProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void createMaterialFinancialProcessInformationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = materialFinancialProcessInformationRepository.findAll().size();

        // Create the MaterialFinancialProcessInformation with an existing ID
        materialFinancialProcessInformation.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMaterialFinancialProcessInformationMockMvc.perform(post("/api/material-financial-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialFinancialProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialFinancialProcessInformation in the database
        List<MaterialFinancialProcessInformation> materialFinancialProcessInformationList = materialFinancialProcessInformationRepository.findAll();
        assertThat(materialFinancialProcessInformationList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMaterialFinancialProcessInformations() throws Exception {
        // Initialize the database
        materialFinancialProcessInformationRepository.saveAndFlush(materialFinancialProcessInformation);

        // Get all the materialFinancialProcessInformationList
        restMaterialFinancialProcessInformationMockMvc.perform(get("/api/material-financial-process-informations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(materialFinancialProcessInformation.getId().intValue())))
            .andExpect(jsonPath("$.[*].companyID").value(hasItem(DEFAULT_COMPANY_ID.toString())))
            .andExpect(jsonPath("$.[*].permanentEstablishmentID").value(hasItem(DEFAULT_PERMANENT_ESTABLISHMENT_ID.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCode").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString())))
            .andExpect(jsonPath("$.[*].lifeCycleStatusCodeText").value(hasItem(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString())));
    }
    
    @Test
    @Transactional
    public void getMaterialFinancialProcessInformation() throws Exception {
        // Initialize the database
        materialFinancialProcessInformationRepository.saveAndFlush(materialFinancialProcessInformation);

        // Get the materialFinancialProcessInformation
        restMaterialFinancialProcessInformationMockMvc.perform(get("/api/material-financial-process-informations/{id}", materialFinancialProcessInformation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(materialFinancialProcessInformation.getId().intValue()))
            .andExpect(jsonPath("$.companyID").value(DEFAULT_COMPANY_ID.toString()))
            .andExpect(jsonPath("$.permanentEstablishmentID").value(DEFAULT_PERMANENT_ESTABLISHMENT_ID.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCode").value(DEFAULT_LIFE_CYCLE_STATUS_CODE.toString()))
            .andExpect(jsonPath("$.lifeCycleStatusCodeText").value(DEFAULT_LIFE_CYCLE_STATUS_CODE_TEXT.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMaterialFinancialProcessInformation() throws Exception {
        // Get the materialFinancialProcessInformation
        restMaterialFinancialProcessInformationMockMvc.perform(get("/api/material-financial-process-informations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMaterialFinancialProcessInformation() throws Exception {
        // Initialize the database
        materialFinancialProcessInformationService.save(materialFinancialProcessInformation);

        int databaseSizeBeforeUpdate = materialFinancialProcessInformationRepository.findAll().size();

        // Update the materialFinancialProcessInformation
        MaterialFinancialProcessInformation updatedMaterialFinancialProcessInformation = materialFinancialProcessInformationRepository.findById(materialFinancialProcessInformation.getId()).get();
        // Disconnect from session so that the updates on updatedMaterialFinancialProcessInformation are not directly saved in db
        em.detach(updatedMaterialFinancialProcessInformation);
        updatedMaterialFinancialProcessInformation
            .companyID(UPDATED_COMPANY_ID)
            .permanentEstablishmentID(UPDATED_PERMANENT_ESTABLISHMENT_ID)
            .lifeCycleStatusCode(UPDATED_LIFE_CYCLE_STATUS_CODE)
            .lifeCycleStatusCodeText(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);

        restMaterialFinancialProcessInformationMockMvc.perform(put("/api/material-financial-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMaterialFinancialProcessInformation)))
            .andExpect(status().isOk());

        // Validate the MaterialFinancialProcessInformation in the database
        List<MaterialFinancialProcessInformation> materialFinancialProcessInformationList = materialFinancialProcessInformationRepository.findAll();
        assertThat(materialFinancialProcessInformationList).hasSize(databaseSizeBeforeUpdate);
        MaterialFinancialProcessInformation testMaterialFinancialProcessInformation = materialFinancialProcessInformationList.get(materialFinancialProcessInformationList.size() - 1);
        assertThat(testMaterialFinancialProcessInformation.getCompanyID()).isEqualTo(UPDATED_COMPANY_ID);
        assertThat(testMaterialFinancialProcessInformation.getPermanentEstablishmentID()).isEqualTo(UPDATED_PERMANENT_ESTABLISHMENT_ID);
        assertThat(testMaterialFinancialProcessInformation.getLifeCycleStatusCode()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE);
        assertThat(testMaterialFinancialProcessInformation.getLifeCycleStatusCodeText()).isEqualTo(UPDATED_LIFE_CYCLE_STATUS_CODE_TEXT);
    }

    @Test
    @Transactional
    public void updateNonExistingMaterialFinancialProcessInformation() throws Exception {
        int databaseSizeBeforeUpdate = materialFinancialProcessInformationRepository.findAll().size();

        // Create the MaterialFinancialProcessInformation

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMaterialFinancialProcessInformationMockMvc.perform(put("/api/material-financial-process-informations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(materialFinancialProcessInformation)))
            .andExpect(status().isBadRequest());

        // Validate the MaterialFinancialProcessInformation in the database
        List<MaterialFinancialProcessInformation> materialFinancialProcessInformationList = materialFinancialProcessInformationRepository.findAll();
        assertThat(materialFinancialProcessInformationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMaterialFinancialProcessInformation() throws Exception {
        // Initialize the database
        materialFinancialProcessInformationService.save(materialFinancialProcessInformation);

        int databaseSizeBeforeDelete = materialFinancialProcessInformationRepository.findAll().size();

        // Delete the materialFinancialProcessInformation
        restMaterialFinancialProcessInformationMockMvc.perform(delete("/api/material-financial-process-informations/{id}", materialFinancialProcessInformation.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database is empty
        List<MaterialFinancialProcessInformation> materialFinancialProcessInformationList = materialFinancialProcessInformationRepository.findAll();
        assertThat(materialFinancialProcessInformationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MaterialFinancialProcessInformation.class);
        MaterialFinancialProcessInformation materialFinancialProcessInformation1 = new MaterialFinancialProcessInformation();
        materialFinancialProcessInformation1.setId(1L);
        MaterialFinancialProcessInformation materialFinancialProcessInformation2 = new MaterialFinancialProcessInformation();
        materialFinancialProcessInformation2.setId(materialFinancialProcessInformation1.getId());
        assertThat(materialFinancialProcessInformation1).isEqualTo(materialFinancialProcessInformation2);
        materialFinancialProcessInformation2.setId(2L);
        assertThat(materialFinancialProcessInformation1).isNotEqualTo(materialFinancialProcessInformation2);
        materialFinancialProcessInformation1.setId(null);
        assertThat(materialFinancialProcessInformation1).isNotEqualTo(materialFinancialProcessInformation2);
    }
}
