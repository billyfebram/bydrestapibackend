package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialDeviantTaxClassification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialDeviantTaxClassificationRepository extends JpaRepository<MaterialDeviantTaxClassification, Long> {

}
