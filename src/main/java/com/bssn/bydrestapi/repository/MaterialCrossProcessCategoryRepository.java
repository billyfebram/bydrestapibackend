package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialCrossProcessCategory;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialCrossProcessCategory entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialCrossProcessCategoryRepository extends JpaRepository<MaterialCrossProcessCategory, Long> {

}
