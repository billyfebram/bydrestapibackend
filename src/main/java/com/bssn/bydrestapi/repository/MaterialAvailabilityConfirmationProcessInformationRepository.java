package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialAvailabilityConfirmationProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialAvailabilityConfirmationProcessInformationRepository extends JpaRepository<MaterialAvailabilityConfirmationProcessInformation, Long> {

}
