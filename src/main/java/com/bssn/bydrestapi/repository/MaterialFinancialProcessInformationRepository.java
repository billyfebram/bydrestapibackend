package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialFinancialProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialFinancialProcessInformationRepository extends JpaRepository<MaterialFinancialProcessInformation, Long> {

}
