package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialSalesProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialSalesProcessInformationRepository extends JpaRepository<MaterialSalesProcessInformation, Long> {

}
