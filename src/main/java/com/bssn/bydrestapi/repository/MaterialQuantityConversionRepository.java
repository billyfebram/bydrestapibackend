package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialQuantityConversion;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialQuantityConversion entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialQuantityConversionRepository extends JpaRepository<MaterialQuantityConversion, Long> {

}
