package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialSupplyPlanningProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialSupplyPlanningProcessInformationRepository extends JpaRepository<MaterialSupplyPlanningProcessInformation, Long> {

}
