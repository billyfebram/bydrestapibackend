package com.bssn.bydrestapi.repository;

import com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the MaterialInventoryProcessInformation entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MaterialInventoryProcessInformationRepository extends JpaRepository<MaterialInventoryProcessInformation, Long> {

}
