package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialAvailabilityConfirmationProcessInformation}.
 */
public interface MaterialAvailabilityConfirmationProcessInformationService {

    /**
     * Save a materialAvailabilityConfirmationProcessInformation.
     *
     * @param materialAvailabilityConfirmationProcessInformation the entity to save.
     * @return the persisted entity.
     */
    MaterialAvailabilityConfirmationProcessInformation save(MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation);

    /**
     * Get all the materialAvailabilityConfirmationProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialAvailabilityConfirmationProcessInformation> findAll(Pageable pageable);


    /**
     * Get the "id" materialAvailabilityConfirmationProcessInformation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialAvailabilityConfirmationProcessInformation> findOne(Long id);

    /**
     * Delete the "id" materialAvailabilityConfirmationProcessInformation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
