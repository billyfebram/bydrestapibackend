package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialFinancialProcessInformation}.
 */
public interface MaterialFinancialProcessInformationService {

    /**
     * Save a materialFinancialProcessInformation.
     *
     * @param materialFinancialProcessInformation the entity to save.
     * @return the persisted entity.
     */
    MaterialFinancialProcessInformation save(MaterialFinancialProcessInformation materialFinancialProcessInformation);

    /**
     * Get all the materialFinancialProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialFinancialProcessInformation> findAll(Pageable pageable);


    /**
     * Get the "id" materialFinancialProcessInformation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialFinancialProcessInformation> findOne(Long id);

    /**
     * Delete the "id" materialFinancialProcessInformation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
