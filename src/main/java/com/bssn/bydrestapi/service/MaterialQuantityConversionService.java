package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialQuantityConversion;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialQuantityConversion}.
 */
public interface MaterialQuantityConversionService {

    /**
     * Save a materialQuantityConversion.
     *
     * @param materialQuantityConversion the entity to save.
     * @return the persisted entity.
     */
    MaterialQuantityConversion save(MaterialQuantityConversion materialQuantityConversion);

    /**
     * Get all the materialQuantityConversions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialQuantityConversion> findAll(Pageable pageable);


    /**
     * Get the "id" materialQuantityConversion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialQuantityConversion> findOne(Long id);

    /**
     * Delete the "id" materialQuantityConversion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
