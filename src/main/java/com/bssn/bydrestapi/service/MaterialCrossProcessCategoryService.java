package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialCrossProcessCategory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialCrossProcessCategory}.
 */
public interface MaterialCrossProcessCategoryService {

    /**
     * Save a materialCrossProcessCategory.
     *
     * @param materialCrossProcessCategory the entity to save.
     * @return the persisted entity.
     */
    MaterialCrossProcessCategory save(MaterialCrossProcessCategory materialCrossProcessCategory);

    /**
     * Get all the materialCrossProcessCategories.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialCrossProcessCategory> findAll(Pageable pageable);


    /**
     * Get the "id" materialCrossProcessCategory.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialCrossProcessCategory> findOne(Long id);

    /**
     * Delete the "id" materialCrossProcessCategory.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
