package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialSalesProcessInformation}.
 */
public interface MaterialSalesProcessInformationService {

    /**
     * Save a materialSalesProcessInformation.
     *
     * @param materialSalesProcessInformation the entity to save.
     * @return the persisted entity.
     */
    MaterialSalesProcessInformation save(MaterialSalesProcessInformation materialSalesProcessInformation);

    /**
     * Get all the materialSalesProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialSalesProcessInformation> findAll(Pageable pageable);


    /**
     * Get the "id" materialSalesProcessInformation.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialSalesProcessInformation> findOne(Long id);

    /**
     * Delete the "id" materialSalesProcessInformation.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
