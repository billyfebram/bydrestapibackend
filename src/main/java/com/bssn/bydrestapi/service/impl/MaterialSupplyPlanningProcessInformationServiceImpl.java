package com.bssn.bydrestapi.service.impl;

import com.bssn.bydrestapi.service.MaterialSupplyPlanningProcessInformationService;
import com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation;
import com.bssn.bydrestapi.repository.MaterialSupplyPlanningProcessInformationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MaterialSupplyPlanningProcessInformation}.
 */
@Service
@Transactional
public class MaterialSupplyPlanningProcessInformationServiceImpl implements MaterialSupplyPlanningProcessInformationService {

    private final Logger log = LoggerFactory.getLogger(MaterialSupplyPlanningProcessInformationServiceImpl.class);

    private final MaterialSupplyPlanningProcessInformationRepository materialSupplyPlanningProcessInformationRepository;

    public MaterialSupplyPlanningProcessInformationServiceImpl(MaterialSupplyPlanningProcessInformationRepository materialSupplyPlanningProcessInformationRepository) {
        this.materialSupplyPlanningProcessInformationRepository = materialSupplyPlanningProcessInformationRepository;
    }

    /**
     * Save a materialSupplyPlanningProcessInformation.
     *
     * @param materialSupplyPlanningProcessInformation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialSupplyPlanningProcessInformation save(MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation) {
        log.debug("Request to save MaterialSupplyPlanningProcessInformation : {}", materialSupplyPlanningProcessInformation);
        return materialSupplyPlanningProcessInformationRepository.save(materialSupplyPlanningProcessInformation);
    }

    /**
     * Get all the materialSupplyPlanningProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialSupplyPlanningProcessInformation> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialSupplyPlanningProcessInformations");
        return materialSupplyPlanningProcessInformationRepository.findAll(pageable);
    }


    /**
     * Get one materialSupplyPlanningProcessInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialSupplyPlanningProcessInformation> findOne(Long id) {
        log.debug("Request to get MaterialSupplyPlanningProcessInformation : {}", id);
        return materialSupplyPlanningProcessInformationRepository.findById(id);
    }

    /**
     * Delete the materialSupplyPlanningProcessInformation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialSupplyPlanningProcessInformation : {}", id);
        materialSupplyPlanningProcessInformationRepository.deleteById(id);
    }
}
