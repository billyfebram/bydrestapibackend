package com.bssn.bydrestapi.service.impl;

import com.bssn.bydrestapi.service.MaterialQuantityConversionService;
import com.bssn.bydrestapi.domain.MaterialQuantityConversion;
import com.bssn.bydrestapi.repository.MaterialQuantityConversionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MaterialQuantityConversion}.
 */
@Service
@Transactional
public class MaterialQuantityConversionServiceImpl implements MaterialQuantityConversionService {

    private final Logger log = LoggerFactory.getLogger(MaterialQuantityConversionServiceImpl.class);

    private final MaterialQuantityConversionRepository materialQuantityConversionRepository;

    public MaterialQuantityConversionServiceImpl(MaterialQuantityConversionRepository materialQuantityConversionRepository) {
        this.materialQuantityConversionRepository = materialQuantityConversionRepository;
    }

    /**
     * Save a materialQuantityConversion.
     *
     * @param materialQuantityConversion the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialQuantityConversion save(MaterialQuantityConversion materialQuantityConversion) {
        log.debug("Request to save MaterialQuantityConversion : {}", materialQuantityConversion);
        return materialQuantityConversionRepository.save(materialQuantityConversion);
    }

    /**
     * Get all the materialQuantityConversions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialQuantityConversion> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialQuantityConversions");
        return materialQuantityConversionRepository.findAll(pageable);
    }


    /**
     * Get one materialQuantityConversion by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialQuantityConversion> findOne(Long id) {
        log.debug("Request to get MaterialQuantityConversion : {}", id);
        return materialQuantityConversionRepository.findById(id);
    }

    /**
     * Delete the materialQuantityConversion by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialQuantityConversion : {}", id);
        materialQuantityConversionRepository.deleteById(id);
    }
}
