package com.bssn.bydrestapi.service.impl;

import com.bssn.bydrestapi.service.MaterialSalesProcessInformationService;
import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;
import com.bssn.bydrestapi.repository.MaterialSalesProcessInformationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MaterialSalesProcessInformation}.
 */
@Service
@Transactional
public class MaterialSalesProcessInformationServiceImpl implements MaterialSalesProcessInformationService {

    private final Logger log = LoggerFactory.getLogger(MaterialSalesProcessInformationServiceImpl.class);

    private final MaterialSalesProcessInformationRepository materialSalesProcessInformationRepository;

    public MaterialSalesProcessInformationServiceImpl(MaterialSalesProcessInformationRepository materialSalesProcessInformationRepository) {
        this.materialSalesProcessInformationRepository = materialSalesProcessInformationRepository;
    }

    /**
     * Save a materialSalesProcessInformation.
     *
     * @param materialSalesProcessInformation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialSalesProcessInformation save(MaterialSalesProcessInformation materialSalesProcessInformation) {
        log.debug("Request to save MaterialSalesProcessInformation : {}", materialSalesProcessInformation);
        return materialSalesProcessInformationRepository.save(materialSalesProcessInformation);
    }

    /**
     * Get all the materialSalesProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialSalesProcessInformation> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialSalesProcessInformations");
        return materialSalesProcessInformationRepository.findAll(pageable);
    }


    /**
     * Get one materialSalesProcessInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialSalesProcessInformation> findOne(Long id) {
        log.debug("Request to get MaterialSalesProcessInformation : {}", id);
        return materialSalesProcessInformationRepository.findById(id);
    }

    /**
     * Delete the materialSalesProcessInformation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialSalesProcessInformation : {}", id);
        materialSalesProcessInformationRepository.deleteById(id);
    }
}
