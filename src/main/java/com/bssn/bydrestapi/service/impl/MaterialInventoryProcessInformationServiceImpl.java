package com.bssn.bydrestapi.service.impl;

import com.bssn.bydrestapi.service.MaterialInventoryProcessInformationService;
import com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation;
import com.bssn.bydrestapi.repository.MaterialInventoryProcessInformationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing {@link MaterialInventoryProcessInformation}.
 */
@Service
@Transactional
public class MaterialInventoryProcessInformationServiceImpl implements MaterialInventoryProcessInformationService {

    private final Logger log = LoggerFactory.getLogger(MaterialInventoryProcessInformationServiceImpl.class);

    private final MaterialInventoryProcessInformationRepository materialInventoryProcessInformationRepository;

    public MaterialInventoryProcessInformationServiceImpl(MaterialInventoryProcessInformationRepository materialInventoryProcessInformationRepository) {
        this.materialInventoryProcessInformationRepository = materialInventoryProcessInformationRepository;
    }

    /**
     * Save a materialInventoryProcessInformation.
     *
     * @param materialInventoryProcessInformation the entity to save.
     * @return the persisted entity.
     */
    @Override
    public MaterialInventoryProcessInformation save(MaterialInventoryProcessInformation materialInventoryProcessInformation) {
        log.debug("Request to save MaterialInventoryProcessInformation : {}", materialInventoryProcessInformation);
        return materialInventoryProcessInformationRepository.save(materialInventoryProcessInformation);
    }

    /**
     * Get all the materialInventoryProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<MaterialInventoryProcessInformation> findAll(Pageable pageable) {
        log.debug("Request to get all MaterialInventoryProcessInformations");
        return materialInventoryProcessInformationRepository.findAll(pageable);
    }


    /**
     * Get one materialInventoryProcessInformation by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<MaterialInventoryProcessInformation> findOne(Long id) {
        log.debug("Request to get MaterialInventoryProcessInformation : {}", id);
        return materialInventoryProcessInformationRepository.findById(id);
    }

    /**
     * Delete the materialInventoryProcessInformation by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete MaterialInventoryProcessInformation : {}", id);
        materialInventoryProcessInformationRepository.deleteById(id);
    }
}
