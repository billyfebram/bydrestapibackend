package com.bssn.bydrestapi.service;

import com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing {@link MaterialDeviantTaxClassification}.
 */
public interface MaterialDeviantTaxClassificationService {

    /**
     * Save a materialDeviantTaxClassification.
     *
     * @param materialDeviantTaxClassification the entity to save.
     * @return the persisted entity.
     */
    MaterialDeviantTaxClassification save(MaterialDeviantTaxClassification materialDeviantTaxClassification);

    /**
     * Get all the materialDeviantTaxClassifications.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<MaterialDeviantTaxClassification> findAll(Pageable pageable);


    /**
     * Get the "id" materialDeviantTaxClassification.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MaterialDeviantTaxClassification> findOne(Long id);

    /**
     * Delete the "id" materialDeviantTaxClassification.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
