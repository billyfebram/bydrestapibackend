package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation;
import com.bssn.bydrestapi.service.MaterialSupplyPlanningProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation}.
 */
@RestController
@RequestMapping("/api")
public class MaterialSupplyPlanningProcessInformationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialSupplyPlanningProcessInformationResource.class);

    private static final String ENTITY_NAME = "materialSupplyPlanningProcessInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialSupplyPlanningProcessInformationService materialSupplyPlanningProcessInformationService;

    public MaterialSupplyPlanningProcessInformationResource(MaterialSupplyPlanningProcessInformationService materialSupplyPlanningProcessInformationService) {
        this.materialSupplyPlanningProcessInformationService = materialSupplyPlanningProcessInformationService;
    }

    /**
     * {@code POST  /material-supply-planning-process-informations} : Create a new materialSupplyPlanningProcessInformation.
     *
     * @param materialSupplyPlanningProcessInformation the materialSupplyPlanningProcessInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialSupplyPlanningProcessInformation, or with status {@code 400 (Bad Request)} if the materialSupplyPlanningProcessInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-supply-planning-process-informations")
    public ResponseEntity<MaterialSupplyPlanningProcessInformation> createMaterialSupplyPlanningProcessInformation(@RequestBody MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation) throws URISyntaxException {
        log.debug("REST request to save MaterialSupplyPlanningProcessInformation : {}", materialSupplyPlanningProcessInformation);
        if (materialSupplyPlanningProcessInformation.getId() != null) {
            throw new BadRequestAlertException("A new materialSupplyPlanningProcessInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialSupplyPlanningProcessInformation result = materialSupplyPlanningProcessInformationService.save(materialSupplyPlanningProcessInformation);
        return ResponseEntity.created(new URI("/api/material-supply-planning-process-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-supply-planning-process-informations} : Updates an existing materialSupplyPlanningProcessInformation.
     *
     * @param materialSupplyPlanningProcessInformation the materialSupplyPlanningProcessInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialSupplyPlanningProcessInformation,
     * or with status {@code 400 (Bad Request)} if the materialSupplyPlanningProcessInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialSupplyPlanningProcessInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-supply-planning-process-informations")
    public ResponseEntity<MaterialSupplyPlanningProcessInformation> updateMaterialSupplyPlanningProcessInformation(@RequestBody MaterialSupplyPlanningProcessInformation materialSupplyPlanningProcessInformation) throws URISyntaxException {
        log.debug("REST request to update MaterialSupplyPlanningProcessInformation : {}", materialSupplyPlanningProcessInformation);
        if (materialSupplyPlanningProcessInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialSupplyPlanningProcessInformation result = materialSupplyPlanningProcessInformationService.save(materialSupplyPlanningProcessInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialSupplyPlanningProcessInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-supply-planning-process-informations} : get all the materialSupplyPlanningProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialSupplyPlanningProcessInformations in body.
     */
    @GetMapping("/material-supply-planning-process-informations")
    public ResponseEntity<List<MaterialSupplyPlanningProcessInformation>> getAllMaterialSupplyPlanningProcessInformations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialSupplyPlanningProcessInformations");
        Page<MaterialSupplyPlanningProcessInformation> page = materialSupplyPlanningProcessInformationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-supply-planning-process-informations/:id} : get the "id" materialSupplyPlanningProcessInformation.
     *
     * @param id the id of the materialSupplyPlanningProcessInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialSupplyPlanningProcessInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-supply-planning-process-informations/{id}")
    public ResponseEntity<MaterialSupplyPlanningProcessInformation> getMaterialSupplyPlanningProcessInformation(@PathVariable Long id) {
        log.debug("REST request to get MaterialSupplyPlanningProcessInformation : {}", id);
        Optional<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformation = materialSupplyPlanningProcessInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialSupplyPlanningProcessInformation);
    }

    /**
     * {@code DELETE  /material-supply-planning-process-informations/:id} : delete the "id" materialSupplyPlanningProcessInformation.
     *
     * @param id the id of the materialSupplyPlanningProcessInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-supply-planning-process-informations/{id}")
    public ResponseEntity<Void> deleteMaterialSupplyPlanningProcessInformation(@PathVariable Long id) {
        log.debug("REST request to delete MaterialSupplyPlanningProcessInformation : {}", id);
        materialSupplyPlanningProcessInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
