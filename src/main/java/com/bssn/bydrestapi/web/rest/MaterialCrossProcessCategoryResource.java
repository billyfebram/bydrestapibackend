package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialCrossProcessCategory;
import com.bssn.bydrestapi.service.MaterialCrossProcessCategoryService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialCrossProcessCategory}.
 */
@RestController
@RequestMapping("/api")
public class MaterialCrossProcessCategoryResource {

    private final Logger log = LoggerFactory.getLogger(MaterialCrossProcessCategoryResource.class);

    private static final String ENTITY_NAME = "materialCrossProcessCategory";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialCrossProcessCategoryService materialCrossProcessCategoryService;

    public MaterialCrossProcessCategoryResource(MaterialCrossProcessCategoryService materialCrossProcessCategoryService) {
        this.materialCrossProcessCategoryService = materialCrossProcessCategoryService;
    }

    /**
     * {@code POST  /material-cross-process-categories} : Create a new materialCrossProcessCategory.
     *
     * @param materialCrossProcessCategory the materialCrossProcessCategory to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialCrossProcessCategory, or with status {@code 400 (Bad Request)} if the materialCrossProcessCategory has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-cross-process-categories")
    public ResponseEntity<MaterialCrossProcessCategory> createMaterialCrossProcessCategory(@RequestBody MaterialCrossProcessCategory materialCrossProcessCategory) throws URISyntaxException {
        log.debug("REST request to save MaterialCrossProcessCategory : {}", materialCrossProcessCategory);
        if (materialCrossProcessCategory.getId() != null) {
            throw new BadRequestAlertException("A new materialCrossProcessCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialCrossProcessCategory result = materialCrossProcessCategoryService.save(materialCrossProcessCategory);
        return ResponseEntity.created(new URI("/api/material-cross-process-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-cross-process-categories} : Updates an existing materialCrossProcessCategory.
     *
     * @param materialCrossProcessCategory the materialCrossProcessCategory to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialCrossProcessCategory,
     * or with status {@code 400 (Bad Request)} if the materialCrossProcessCategory is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialCrossProcessCategory couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-cross-process-categories")
    public ResponseEntity<MaterialCrossProcessCategory> updateMaterialCrossProcessCategory(@RequestBody MaterialCrossProcessCategory materialCrossProcessCategory) throws URISyntaxException {
        log.debug("REST request to update MaterialCrossProcessCategory : {}", materialCrossProcessCategory);
        if (materialCrossProcessCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialCrossProcessCategory result = materialCrossProcessCategoryService.save(materialCrossProcessCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialCrossProcessCategory.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-cross-process-categories} : get all the materialCrossProcessCategories.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialCrossProcessCategories in body.
     */
    @GetMapping("/material-cross-process-categories")
    public ResponseEntity<List<MaterialCrossProcessCategory>> getAllMaterialCrossProcessCategories(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialCrossProcessCategories");
        Page<MaterialCrossProcessCategory> page = materialCrossProcessCategoryService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-cross-process-categories/:id} : get the "id" materialCrossProcessCategory.
     *
     * @param id the id of the materialCrossProcessCategory to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialCrossProcessCategory, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-cross-process-categories/{id}")
    public ResponseEntity<MaterialCrossProcessCategory> getMaterialCrossProcessCategory(@PathVariable Long id) {
        log.debug("REST request to get MaterialCrossProcessCategory : {}", id);
        Optional<MaterialCrossProcessCategory> materialCrossProcessCategory = materialCrossProcessCategoryService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialCrossProcessCategory);
    }

    /**
     * {@code DELETE  /material-cross-process-categories/:id} : delete the "id" materialCrossProcessCategory.
     *
     * @param id the id of the materialCrossProcessCategory to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-cross-process-categories/{id}")
    public ResponseEntity<Void> deleteMaterialCrossProcessCategory(@PathVariable Long id) {
        log.debug("REST request to delete MaterialCrossProcessCategory : {}", id);
        materialCrossProcessCategoryService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
