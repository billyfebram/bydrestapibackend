package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialQuantityConversion;
import com.bssn.bydrestapi.service.MaterialQuantityConversionService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialQuantityConversion}.
 */
@RestController
@RequestMapping("/api")
public class MaterialQuantityConversionResource {

    private final Logger log = LoggerFactory.getLogger(MaterialQuantityConversionResource.class);

    private static final String ENTITY_NAME = "materialQuantityConversion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialQuantityConversionService materialQuantityConversionService;

    public MaterialQuantityConversionResource(MaterialQuantityConversionService materialQuantityConversionService) {
        this.materialQuantityConversionService = materialQuantityConversionService;
    }

    /**
     * {@code POST  /material-quantity-conversions} : Create a new materialQuantityConversion.
     *
     * @param materialQuantityConversion the materialQuantityConversion to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialQuantityConversion, or with status {@code 400 (Bad Request)} if the materialQuantityConversion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-quantity-conversions")
    public ResponseEntity<MaterialQuantityConversion> createMaterialQuantityConversion(@RequestBody MaterialQuantityConversion materialQuantityConversion) throws URISyntaxException {
        log.debug("REST request to save MaterialQuantityConversion : {}", materialQuantityConversion);
        if (materialQuantityConversion.getId() != null) {
            throw new BadRequestAlertException("A new materialQuantityConversion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialQuantityConversion result = materialQuantityConversionService.save(materialQuantityConversion);
        return ResponseEntity.created(new URI("/api/material-quantity-conversions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-quantity-conversions} : Updates an existing materialQuantityConversion.
     *
     * @param materialQuantityConversion the materialQuantityConversion to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialQuantityConversion,
     * or with status {@code 400 (Bad Request)} if the materialQuantityConversion is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialQuantityConversion couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-quantity-conversions")
    public ResponseEntity<MaterialQuantityConversion> updateMaterialQuantityConversion(@RequestBody MaterialQuantityConversion materialQuantityConversion) throws URISyntaxException {
        log.debug("REST request to update MaterialQuantityConversion : {}", materialQuantityConversion);
        if (materialQuantityConversion.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialQuantityConversion result = materialQuantityConversionService.save(materialQuantityConversion);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialQuantityConversion.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-quantity-conversions} : get all the materialQuantityConversions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialQuantityConversions in body.
     */
    @GetMapping("/material-quantity-conversions")
    public ResponseEntity<List<MaterialQuantityConversion>> getAllMaterialQuantityConversions(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialQuantityConversions");
        Page<MaterialQuantityConversion> page = materialQuantityConversionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-quantity-conversions/:id} : get the "id" materialQuantityConversion.
     *
     * @param id the id of the materialQuantityConversion to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialQuantityConversion, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-quantity-conversions/{id}")
    public ResponseEntity<MaterialQuantityConversion> getMaterialQuantityConversion(@PathVariable Long id) {
        log.debug("REST request to get MaterialQuantityConversion : {}", id);
        Optional<MaterialQuantityConversion> materialQuantityConversion = materialQuantityConversionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialQuantityConversion);
    }

    /**
     * {@code DELETE  /material-quantity-conversions/:id} : delete the "id" materialQuantityConversion.
     *
     * @param id the id of the materialQuantityConversion to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-quantity-conversions/{id}")
    public ResponseEntity<Void> deleteMaterialQuantityConversion(@PathVariable Long id) {
        log.debug("REST request to delete MaterialQuantityConversion : {}", id);
        materialQuantityConversionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
