package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification;
import com.bssn.bydrestapi.service.MaterialDeviantTaxClassificationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification}.
 */
@RestController
@RequestMapping("/api")
public class MaterialDeviantTaxClassificationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialDeviantTaxClassificationResource.class);

    private static final String ENTITY_NAME = "materialDeviantTaxClassification";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialDeviantTaxClassificationService materialDeviantTaxClassificationService;

    public MaterialDeviantTaxClassificationResource(MaterialDeviantTaxClassificationService materialDeviantTaxClassificationService) {
        this.materialDeviantTaxClassificationService = materialDeviantTaxClassificationService;
    }

    /**
     * {@code POST  /material-deviant-tax-classifications} : Create a new materialDeviantTaxClassification.
     *
     * @param materialDeviantTaxClassification the materialDeviantTaxClassification to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialDeviantTaxClassification, or with status {@code 400 (Bad Request)} if the materialDeviantTaxClassification has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-deviant-tax-classifications")
    public ResponseEntity<MaterialDeviantTaxClassification> createMaterialDeviantTaxClassification(@RequestBody MaterialDeviantTaxClassification materialDeviantTaxClassification) throws URISyntaxException {
        log.debug("REST request to save MaterialDeviantTaxClassification : {}", materialDeviantTaxClassification);
        if (materialDeviantTaxClassification.getId() != null) {
            throw new BadRequestAlertException("A new materialDeviantTaxClassification cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialDeviantTaxClassification result = materialDeviantTaxClassificationService.save(materialDeviantTaxClassification);
        return ResponseEntity.created(new URI("/api/material-deviant-tax-classifications/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-deviant-tax-classifications} : Updates an existing materialDeviantTaxClassification.
     *
     * @param materialDeviantTaxClassification the materialDeviantTaxClassification to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialDeviantTaxClassification,
     * or with status {@code 400 (Bad Request)} if the materialDeviantTaxClassification is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialDeviantTaxClassification couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-deviant-tax-classifications")
    public ResponseEntity<MaterialDeviantTaxClassification> updateMaterialDeviantTaxClassification(@RequestBody MaterialDeviantTaxClassification materialDeviantTaxClassification) throws URISyntaxException {
        log.debug("REST request to update MaterialDeviantTaxClassification : {}", materialDeviantTaxClassification);
        if (materialDeviantTaxClassification.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialDeviantTaxClassification result = materialDeviantTaxClassificationService.save(materialDeviantTaxClassification);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialDeviantTaxClassification.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-deviant-tax-classifications} : get all the materialDeviantTaxClassifications.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialDeviantTaxClassifications in body.
     */
    @GetMapping("/material-deviant-tax-classifications")
    public ResponseEntity<List<MaterialDeviantTaxClassification>> getAllMaterialDeviantTaxClassifications(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialDeviantTaxClassifications");
        Page<MaterialDeviantTaxClassification> page = materialDeviantTaxClassificationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-deviant-tax-classifications/:id} : get the "id" materialDeviantTaxClassification.
     *
     * @param id the id of the materialDeviantTaxClassification to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialDeviantTaxClassification, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-deviant-tax-classifications/{id}")
    public ResponseEntity<MaterialDeviantTaxClassification> getMaterialDeviantTaxClassification(@PathVariable Long id) {
        log.debug("REST request to get MaterialDeviantTaxClassification : {}", id);
        Optional<MaterialDeviantTaxClassification> materialDeviantTaxClassification = materialDeviantTaxClassificationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialDeviantTaxClassification);
    }

    /**
     * {@code DELETE  /material-deviant-tax-classifications/:id} : delete the "id" materialDeviantTaxClassification.
     *
     * @param id the id of the materialDeviantTaxClassification to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-deviant-tax-classifications/{id}")
    public ResponseEntity<Void> deleteMaterialDeviantTaxClassification(@PathVariable Long id) {
        log.debug("REST request to delete MaterialDeviantTaxClassification : {}", id);
        materialDeviantTaxClassificationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
