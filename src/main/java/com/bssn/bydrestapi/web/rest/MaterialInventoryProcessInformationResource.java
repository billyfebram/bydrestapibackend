package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation;
import com.bssn.bydrestapi.service.MaterialInventoryProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation}.
 */
@RestController
@RequestMapping("/api")
public class MaterialInventoryProcessInformationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialInventoryProcessInformationResource.class);

    private static final String ENTITY_NAME = "materialInventoryProcessInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialInventoryProcessInformationService materialInventoryProcessInformationService;

    public MaterialInventoryProcessInformationResource(MaterialInventoryProcessInformationService materialInventoryProcessInformationService) {
        this.materialInventoryProcessInformationService = materialInventoryProcessInformationService;
    }

    /**
     * {@code POST  /material-inventory-process-informations} : Create a new materialInventoryProcessInformation.
     *
     * @param materialInventoryProcessInformation the materialInventoryProcessInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialInventoryProcessInformation, or with status {@code 400 (Bad Request)} if the materialInventoryProcessInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-inventory-process-informations")
    public ResponseEntity<MaterialInventoryProcessInformation> createMaterialInventoryProcessInformation(@RequestBody MaterialInventoryProcessInformation materialInventoryProcessInformation) throws URISyntaxException {
        log.debug("REST request to save MaterialInventoryProcessInformation : {}", materialInventoryProcessInformation);
        if (materialInventoryProcessInformation.getId() != null) {
            throw new BadRequestAlertException("A new materialInventoryProcessInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialInventoryProcessInformation result = materialInventoryProcessInformationService.save(materialInventoryProcessInformation);
        return ResponseEntity.created(new URI("/api/material-inventory-process-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-inventory-process-informations} : Updates an existing materialInventoryProcessInformation.
     *
     * @param materialInventoryProcessInformation the materialInventoryProcessInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialInventoryProcessInformation,
     * or with status {@code 400 (Bad Request)} if the materialInventoryProcessInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialInventoryProcessInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-inventory-process-informations")
    public ResponseEntity<MaterialInventoryProcessInformation> updateMaterialInventoryProcessInformation(@RequestBody MaterialInventoryProcessInformation materialInventoryProcessInformation) throws URISyntaxException {
        log.debug("REST request to update MaterialInventoryProcessInformation : {}", materialInventoryProcessInformation);
        if (materialInventoryProcessInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialInventoryProcessInformation result = materialInventoryProcessInformationService.save(materialInventoryProcessInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialInventoryProcessInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-inventory-process-informations} : get all the materialInventoryProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialInventoryProcessInformations in body.
     */
    @GetMapping("/material-inventory-process-informations")
    public ResponseEntity<List<MaterialInventoryProcessInformation>> getAllMaterialInventoryProcessInformations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialInventoryProcessInformations");
        Page<MaterialInventoryProcessInformation> page = materialInventoryProcessInformationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-inventory-process-informations/:id} : get the "id" materialInventoryProcessInformation.
     *
     * @param id the id of the materialInventoryProcessInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialInventoryProcessInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-inventory-process-informations/{id}")
    public ResponseEntity<MaterialInventoryProcessInformation> getMaterialInventoryProcessInformation(@PathVariable Long id) {
        log.debug("REST request to get MaterialInventoryProcessInformation : {}", id);
        Optional<MaterialInventoryProcessInformation> materialInventoryProcessInformation = materialInventoryProcessInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialInventoryProcessInformation);
    }

    /**
     * {@code DELETE  /material-inventory-process-informations/:id} : delete the "id" materialInventoryProcessInformation.
     *
     * @param id the id of the materialInventoryProcessInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-inventory-process-informations/{id}")
    public ResponseEntity<Void> deleteMaterialInventoryProcessInformation(@PathVariable Long id) {
        log.debug("REST request to delete MaterialInventoryProcessInformation : {}", id);
        materialInventoryProcessInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
