/**
 * View Models used by Spring MVC REST controllers.
 */
package com.bssn.bydrestapi.web.rest.vm;
