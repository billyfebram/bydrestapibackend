package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation;
import com.bssn.bydrestapi.service.MaterialFinancialProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation}.
 */
@RestController
@RequestMapping("/api")
public class MaterialFinancialProcessInformationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialFinancialProcessInformationResource.class);

    private static final String ENTITY_NAME = "materialFinancialProcessInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialFinancialProcessInformationService materialFinancialProcessInformationService;

    public MaterialFinancialProcessInformationResource(MaterialFinancialProcessInformationService materialFinancialProcessInformationService) {
        this.materialFinancialProcessInformationService = materialFinancialProcessInformationService;
    }

    /**
     * {@code POST  /material-financial-process-informations} : Create a new materialFinancialProcessInformation.
     *
     * @param materialFinancialProcessInformation the materialFinancialProcessInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialFinancialProcessInformation, or with status {@code 400 (Bad Request)} if the materialFinancialProcessInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-financial-process-informations")
    public ResponseEntity<MaterialFinancialProcessInformation> createMaterialFinancialProcessInformation(@RequestBody MaterialFinancialProcessInformation materialFinancialProcessInformation) throws URISyntaxException {
        log.debug("REST request to save MaterialFinancialProcessInformation : {}", materialFinancialProcessInformation);
        if (materialFinancialProcessInformation.getId() != null) {
            throw new BadRequestAlertException("A new materialFinancialProcessInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialFinancialProcessInformation result = materialFinancialProcessInformationService.save(materialFinancialProcessInformation);
        return ResponseEntity.created(new URI("/api/material-financial-process-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-financial-process-informations} : Updates an existing materialFinancialProcessInformation.
     *
     * @param materialFinancialProcessInformation the materialFinancialProcessInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialFinancialProcessInformation,
     * or with status {@code 400 (Bad Request)} if the materialFinancialProcessInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialFinancialProcessInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-financial-process-informations")
    public ResponseEntity<MaterialFinancialProcessInformation> updateMaterialFinancialProcessInformation(@RequestBody MaterialFinancialProcessInformation materialFinancialProcessInformation) throws URISyntaxException {
        log.debug("REST request to update MaterialFinancialProcessInformation : {}", materialFinancialProcessInformation);
        if (materialFinancialProcessInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialFinancialProcessInformation result = materialFinancialProcessInformationService.save(materialFinancialProcessInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialFinancialProcessInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-financial-process-informations} : get all the materialFinancialProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialFinancialProcessInformations in body.
     */
    @GetMapping("/material-financial-process-informations")
    public ResponseEntity<List<MaterialFinancialProcessInformation>> getAllMaterialFinancialProcessInformations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialFinancialProcessInformations");
        Page<MaterialFinancialProcessInformation> page = materialFinancialProcessInformationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-financial-process-informations/:id} : get the "id" materialFinancialProcessInformation.
     *
     * @param id the id of the materialFinancialProcessInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialFinancialProcessInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-financial-process-informations/{id}")
    public ResponseEntity<MaterialFinancialProcessInformation> getMaterialFinancialProcessInformation(@PathVariable Long id) {
        log.debug("REST request to get MaterialFinancialProcessInformation : {}", id);
        Optional<MaterialFinancialProcessInformation> materialFinancialProcessInformation = materialFinancialProcessInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialFinancialProcessInformation);
    }

    /**
     * {@code DELETE  /material-financial-process-informations/:id} : delete the "id" materialFinancialProcessInformation.
     *
     * @param id the id of the materialFinancialProcessInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-financial-process-informations/{id}")
    public ResponseEntity<Void> deleteMaterialFinancialProcessInformation(@PathVariable Long id) {
        log.debug("REST request to delete MaterialFinancialProcessInformation : {}", id);
        materialFinancialProcessInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
