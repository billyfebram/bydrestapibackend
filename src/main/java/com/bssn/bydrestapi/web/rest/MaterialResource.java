package com.bssn.bydrestapi.web.rest;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.olingo.odata2.api.commons.HttpStatusCodes;
import org.apache.olingo.odata2.api.edm.Edm;
import org.apache.olingo.odata2.api.edm.EdmEntityContainer;
import org.apache.olingo.odata2.api.ep.EntityProvider;
import org.apache.olingo.odata2.api.ep.EntityProviderReadProperties;
import org.apache.olingo.odata2.api.ep.entry.ODataEntry;
import org.apache.olingo.odata2.api.ep.feed.ODataFeed;
import org.apache.olingo.odata2.api.exception.ODataException;
import org.apache.olingo.odata2.core.ep.entry.ODataEntryImpl;
import org.apache.olingo.odata2.core.ep.feed.ODataDeltaFeedImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.bssn.bydrestapi.domain.Material;
import com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation;
import com.bssn.bydrestapi.domain.MaterialCrossProcessCategory;
import com.bssn.bydrestapi.domain.MaterialDeviantTaxClassification;
import com.bssn.bydrestapi.domain.MaterialFinancialProcessInformation;
import com.bssn.bydrestapi.domain.MaterialInventoryProcessInformation;
import com.bssn.bydrestapi.domain.MaterialQuantityConversion;
import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;
import com.bssn.bydrestapi.domain.MaterialSupplyPlanningProcessInformation;
import com.bssn.bydrestapi.service.MaterialAvailabilityConfirmationProcessInformationService;
import com.bssn.bydrestapi.service.MaterialCrossProcessCategoryService;
import com.bssn.bydrestapi.service.MaterialDeviantTaxClassificationService;
import com.bssn.bydrestapi.service.MaterialFinancialProcessInformationService;
import com.bssn.bydrestapi.service.MaterialInventoryProcessInformationService;
import com.bssn.bydrestapi.service.MaterialQuantityConversionService;
import com.bssn.bydrestapi.service.MaterialSalesProcessInformationService;
import com.bssn.bydrestapi.service.MaterialService;
import com.bssn.bydrestapi.service.MaterialSupplyPlanningProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.Material}.
 */
@RestController
@RequestMapping("/api")
public class MaterialResource {

    private final Logger log = LoggerFactory.getLogger(MaterialResource.class);

    private static final String ENTITY_NAME = "material";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialService materialService;
    
    @Autowired
    private MaterialDeviantTaxClassificationService materialDeviantTaxClassificationService;
    
    @Autowired
    private MaterialQuantityConversionService materialQuantityConversionService;
    
    @Autowired
    private MaterialCrossProcessCategoryService materialCrossProcessCategoryService;
    
    @Autowired
    private MaterialInventoryProcessInformationService materialInventoryProcessInformationService;
    
    @Autowired
    private MaterialSalesProcessInformationService materialSalesProcessInformationService;
    
    @Autowired
    private MaterialFinancialProcessInformationService materialFinancialProcessInformationService;
    
    @Autowired
    private MaterialSupplyPlanningProcessInformationService materialSupplyPlanningProcessInformationService;
    
    @Autowired
    private MaterialAvailabilityConfirmationProcessInformationService materialAvailabilityConfirmationProcessInformationService;
    
    
    public MaterialResource(MaterialService materialService) {
        this.materialService = materialService;
    }

    /**
     * {@code POST  /materials} : Create a new material.
     *
     * @param material the material to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new material, or with status {@code 400 (Bad Request)} if the material has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/materials")
    public ResponseEntity<Material> createMaterial(@RequestBody Material material) throws URISyntaxException {
        log.debug("REST request to save Material : {}", material);
        if (material.getId() != null) {
            throw new BadRequestAlertException("A new material cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Material result = materialService.save(material);
        return ResponseEntity.created(new URI("/api/materials/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code POST  /materials/load-data} : Load data material.
     *
     * @param material the material to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new material, or with status {@code 400 (Bad Request)} if the material has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     * @throws ODataException 
     * @throws IOException 
     */
    @PostMapping("/materials/load-data")
    public ResponseEntity<Void> loadDataMaterial() throws URISyntaxException, IOException, ODataException {
        log.debug("REST request to load Data Material");
        
        String serviceUrl = "https://my348779.sapbydesign.com/sap/byd/odata/cust/v1/vmumaterial";
        Edm edm = readEdm(serviceUrl);
        EdmEntityContainer entityContainer = edm.getDefaultEntityContainer();
        String url = "https://my348779.sapbydesign.com/sap/byd/odata/cust/v1/vmumaterial/MaterialCollection?$format=json&$expand=MaterialDeviantTaxClassification,MaterialQuantityConversion,MaterialCrossProcessCategory,MaterialInventoryProcessInformation,MaterialSalesProcessInformation,MaterialFinancialProcessInformation,MaterialSupplyPlanningProcessInformation,MaterialAvailabilityConfirmationProcessInformation";
	  	InputStream content = execute(url, APPLICATION_JSON, HTTP_METHOD_GET);
	  	ODataFeed feed =  EntityProvider.readFeed(APPLICATION_JSON,
	  	        entityContainer.getEntitySet("MaterialCollection"),
	  	        content,
	  	        EntityProviderReadProperties.init().build());
	  	
        materialService.deleteAll();        
        for (ODataEntry entry : feed.getEntries()) {
	      	Map<String, Object> data = entry.getProperties();
	      	
	      	Material m = new Material();
	      	m.setProcurementMeasureUnitCode((String)data.get("ProcurementMeasureUnitCode"));
	      	m.setProcurementMeasureUnitCodeText((String)data.get("ProcurementMeasureUnitCodeText"));
	      	m.setDescription((String)data.get("Description"));	      	
	      	m.setProcurementLifeCycleStatusCode((String)data.get("ProcurementLifeCycleStatusCode"));	      	
	      	m.setProcurementLifeCycleStatusCodeText((String)data.get("ProcurementLifeCycleStatusCodeText"));
	      	m.setInternalID((String)data.get("InternalID"));
	      	m.setuUID(((UUID)data.get("UUID")) != null ? data.get("UUID").toString() : null);	      	
	      	m.setBaseMeasureUnitCode((String)data.get("BaseMeasureUnitCode"));
	      	m.setBaseMeasureUnitCodeText((String)data.get("BaseMeasureUnitCodeText"));
	      	m.setIdentifiedStockTypeCode((String)data.get("IdentifiedStockTypeCode"));
	      	m.setIdentifiedStockTypeCodeText((String)data.get("IdentifiedStockTypeCodeText"));
	      	m.setBaseMeasureUnitCodeText((String)data.get("BaseMeasureUnitCodeText"));
	      	m.setLanguageCode((String)data.get("languageCode"));
	      	m.setLanguageCodeText((String)data.get("languageCodeText"));
	      	m.setProductValuationLevelTypeCode((String)data.get("ProductValuationLevelTypeCode"));
	      	m.setProductValuationLevelTypeCodeText((String)data.get("ProductValuationLevelTypeCodeText"));

	      	// MaterialDeviantTaxClassification
	      	Set<MaterialDeviantTaxClassification> materialDeviantTaxClassification = m.getMaterialDeviantTaxClassifications();
	      	ODataDeltaFeedImpl odfi = (ODataDeltaFeedImpl) data.get("MaterialDeviantTaxClassification");
	      	List<ODataEntry> listData = odfi.getEntries();
	      	for(ODataEntry mdtc : listData) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialDeviantTaxClassification mdtcObj = new MaterialDeviantTaxClassification();
	      		mdtcObj.setCountryCode((String)mdtcData.get("CountryCode"));
	      		mdtcObj.setCountryCodeText((String)mdtcData.get("CountryCodeText"));
	      		mdtcObj.setRegionCode((String)mdtcData.get("RegionCode"));
	      		mdtcObj.setRegionCodeText((String)mdtcData.get("RegionCodeText"));
	      		mdtcObj.setTaxExemptionReasonCode((String)mdtcData.get("TaxExemptionReasonCode"));
	      		mdtcObj.setTaxExemptionReasonCodeText((String)mdtcData.get("TaxExemptionReasonCodeText"));
	      		mdtcObj.setTaxRateTypeCode((String)mdtcData.get("TaxRateTypeCode"));
	      		mdtcObj.setTaxRateTypeCodeText((String)mdtcData.get("TaxRateTypeCodeText"));
	      		mdtcObj.setTaxTypeCode((String)mdtcData.get("TaxTypeCode"));
	      		mdtcObj.setTaxRateTypeCodeText((String)mdtcData.get("TaxTypeCodeText"));
	      		
	      		materialDeviantTaxClassificationService.save(mdtcObj);
	      		materialDeviantTaxClassification.add(mdtcObj);
	      	}
	      	// End MaterialDeviantTaxClassification
	      	
	      	// MaterialQuantityConversion
	      	Set<MaterialQuantityConversion> materialQuantityConversion = m.getMaterialQuantityConversions();
	      	ODataDeltaFeedImpl odfi2 = (ODataDeltaFeedImpl) data.get("MaterialQuantityConversion");
	      	List<ODataEntry> listData2 = odfi2.getEntries();
	      	for(ODataEntry mdtc : listData2) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialQuantityConversion mdtcObj = new MaterialQuantityConversion();
	      		mdtcObj.setCorrespondingQuantity((BigDecimal)mdtcData.get("CorrespondingQuantity"));
	      		mdtcObj.setCorrespondingQuantityUnitCode((String)mdtcData.get("CorrespondingQuantityUnitCode"));
	      		mdtcObj.setCorrespondingQuantityUnitCodeText((String)mdtcData.get("CorrespondingQuantityUnitCodeText"));
	      		mdtcObj.setQuantity((BigDecimal)mdtcData.get("Quantity"));
	      		mdtcObj.setQuantityUnitCode((String)mdtcData.get("QuantityUnitCode"));
	      		mdtcObj.setQuantityUnitCodeText((String)mdtcData.get("QuantityUnitCodeText"));
	      		mdtcObj.setBatchDependentIndicator((Boolean)mdtcData.get("BatchDependentIndicator"));
	      		
	      		materialQuantityConversionService.save(mdtcObj);
	      		materialQuantityConversion.add(mdtcObj);
	      	}
	      	// End MaterialQuantityConversion
	      	
	      	// MaterialCrossProcessCategory            
            Set<MaterialCrossProcessCategory> materialCrossProcessCategory = m.getMaterialCrossProcessCategories();
            ODataEntryImpl odfi3 = (ODataEntryImpl) data.get("MaterialCrossProcessCategory");
            
            Map<String, Object> mdtcData2 = odfi3.getProperties();
      		MaterialCrossProcessCategory mcpcObj = new MaterialCrossProcessCategory();
      		mcpcObj.setProductCategoryInternalID((String)mdtcData2.get("ProductCategoryInternalID"));
      		mcpcObj.setDescription((String)mdtcData2.get("Description"));
      		mcpcObj.setDescriptionLanguageCode((String)mdtcData2.get("DescriptionLanguageCode"));
      		mcpcObj.setDescriptionLanguageCodeText((String)mdtcData2.get("DescriptionLanguageCodeText"));
      		
      		materialCrossProcessCategoryService.save(mcpcObj);
      		materialCrossProcessCategory.add(mcpcObj);
//	      	List<ODataEntry> listData3 = odfi3.getEntries();
//	      	for(ODataEntry mdtc : listData3) {
//	      		Map<String, Object> mdtcData = mdtc.getProperties();
//	      		MaterialCrossProcessCategory mdtcObj = new MaterialCrossProcessCategory();
//	      		mdtcObj.setProductCategoryInternalID((String)mdtcData.get("ProductCategoryInternalID"));
//	      		mdtcObj.setDescription((String)mdtcData.get("Description"));
//	      		mdtcObj.setDescriptionLanguageCode((String)mdtcData.get("DescriptionLanguageCode"));
//	      		mdtcObj.setDescriptionLanguageCodeText((String)mdtcData.get("DescriptionLanguageCodeText"));
//	      		
//	      		materialCrossProcessCategoryService.save(mdtcObj);
//	      		materialCrossProcessCategory.add(mdtcObj);
//	      	}
	      	// End MaterialCrossProcessCategory
	      	
	      	// MaterialInventoryProcessInformation
	      	Set<MaterialInventoryProcessInformation> materialInventoryProcessInformation = m.getMaterialInventoryProcessInformations();
	      	ODataDeltaFeedImpl odfi4 = (ODataDeltaFeedImpl) data.get("MaterialInventoryProcessInformation");
	      	List<ODataEntry> listData4 = odfi4.getEntries();
	      	for(ODataEntry mdtc : listData4) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialInventoryProcessInformation mdtcObj = new MaterialInventoryProcessInformation();
	      		mdtcObj.setSiteID((String)mdtcData.get("SiteID"));
	      		mdtcObj.setLifeCycleStatusCode((String)mdtcData.get("LifeCycleStatusCode"));
	      		mdtcObj.setLifeCycleStatusCodeText((String)mdtcData.get("LifeCycleStatusCodeText"));
	      		
	      		materialInventoryProcessInformationService.save(mdtcObj);
	      		materialInventoryProcessInformation.add(mdtcObj);
	      	}
	      	// End MaterialInventoryProcessInformation
	      	
	      	// MaterialSalesProcessInformation
	      	Set<MaterialSalesProcessInformation> materialSalesProcessInformation = m.getMaterialSalesProcessInformations();
	      	ODataDeltaFeedImpl odfi5 = (ODataDeltaFeedImpl) data.get("MaterialSalesProcessInformation");
	      	List<ODataEntry> listData5 = odfi5.getEntries();
	      	for(ODataEntry mdtc : listData5) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialSalesProcessInformation mdtcObj = new MaterialSalesProcessInformation();
	      		mdtcObj.setSalesOrganisationID((String)mdtcData.get("SalesOrganisationID"));
	      		mdtcObj.setDistributionChannelCode((String)mdtcData.get("DistributionChannelCode"));
	      		mdtcObj.setLifeCycleStatusCode((String)mdtcData.get("LifeCycleStatusCode"));
	      		mdtcObj.setLifeCycleStatusCodeText((String)mdtcData.get("LifeCycleStatusCodeText"));
	      		
	      		mdtcObj.setSalesMeasureUnitCode((String)mdtcData.get("SalesMeasureUnitCode"));
	      		mdtcObj.setSalesMeasureUnitCodeText((String)mdtcData.get("SalesMeasureUnitCodeText"));
	      		mdtcObj.setCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode((String)mdtcData.get("CustomerTransactionDocumentItemProcessingTypeDeterminationProductGroupCode"));
	      		mdtcObj.setCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt((String)mdtcData.get("CustomerTransactionDocumentItemProcessingTypeDeterminationProductGroupCodeText"));
	      		mdtcObj.setCashDiscountDeductibleIndicator((Boolean)mdtcData.get("CashDiscountDeductibleIndicator"));
	      		mdtcObj.setMinimumOrderQuantity((BigDecimal)mdtcData.get("MinimumOrderQuantity"));
	      		
	      		materialSalesProcessInformationService.save(mdtcObj);
	      		materialSalesProcessInformation.add(mdtcObj);
	      	}
	      	// End MaterialSalesProcessInformation
	      	
	      	// MaterialFinancialProcessInformation
            Set<MaterialFinancialProcessInformation> materialFinancialProcessInformation = m.getMaterialFinancialProcessInformations();
	      	ODataDeltaFeedImpl odfi6 = (ODataDeltaFeedImpl) data.get("MaterialFinancialProcessInformation");
	      	List<ODataEntry> listData6 = odfi6.getEntries();
	      	for(ODataEntry mdtc : listData6) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialFinancialProcessInformation mdtcObj = new MaterialFinancialProcessInformation();
	      		mdtcObj.setCompanyID((String)mdtcData.get("SiteID"));
	      		mdtcObj.setPermanentEstablishmentID((String)mdtcData.get("PermanentEstablishmentID"));
	      		mdtcObj.setLifeCycleStatusCode((String)mdtcData.get("LifeCycleStatusCode"));
	      		mdtcObj.setLifeCycleStatusCodeText((String)mdtcData.get("LifeCycleStatusCodeText"));
	      		
	      		materialFinancialProcessInformationService.save(mdtcObj);
	      		materialFinancialProcessInformation.add(mdtcObj);
	      	}
	      	// End MaterialFinancialProcessInformation
	      	
	      	// MaterialSupplyPlanningProcessInformation
	      	Set<MaterialSupplyPlanningProcessInformation> materialSupplyPlanningProcessInformation = m.getMaterialSupplyPlanningProcessInformations();
	      	ODataDeltaFeedImpl odfi7 = (ODataDeltaFeedImpl) data.get("MaterialSupplyPlanningProcessInformation");
	      	List<ODataEntry> listData7 = odfi7.getEntries();
	      	for(ODataEntry mdtc : listData7) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialSupplyPlanningProcessInformation mdtcObj = new MaterialSupplyPlanningProcessInformation();
	      		
	      		mdtcObj.setDefaultProcurementMethodCode((String)mdtcData.get("DefaultProcurementMethodCode"));
	      		mdtcObj.setDefaultProcurementMethodCodeText((String)mdtcData.get("DefaultProcurementMethodCodeText"));
	      		mdtcObj.setSupplyPlanningProcedureCode((String)mdtcData.get("SupplyPlanningProcedureCode"));
	      		mdtcObj.setSupplyPlanningProcedureCodeText((String)mdtcData.get("SupplyPlanningProcedureCodeText"));
	      		mdtcObj.setLotSizeProcedureCode((String)mdtcData.get("LotSizeProcedureCode"));
	      		mdtcObj.setLotSizeProcedureCodeText((String)mdtcData.get("LotSizeProcedureCodeText"));
	      		mdtcObj.setSupplyPlanningAreaID((String)mdtcData.get("SupplyPlanningAreaID"));
	      		mdtcObj.setLifeCycleStatusCode((String)mdtcData.get("LifeCycleStatusCode"));
	      		mdtcObj.setLifeCycleStatusCodeText((String)mdtcData.get("LifeCycleStatusCodeText"));
	      		
	      		materialSupplyPlanningProcessInformationService.save(mdtcObj);
	      		materialSupplyPlanningProcessInformation.add(mdtcObj);
	      	}
	      	// End MaterialSupplyPlanningProcessInformation
	      	
	      	// MaterialAvailabilityConfirmationProcessInformation
	      	Set<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformation = m.getMaterialAvailabilityConfirmationProcessInformations();
	      	ODataDeltaFeedImpl odfi8 = (ODataDeltaFeedImpl) data.get("MaterialAvailabilityConfirmationProcessInformation");
	      	List<ODataEntry> listData8 = odfi8.getEntries();
	      	for(ODataEntry mdtc : listData8) {
	      		Map<String, Object> mdtcData = mdtc.getProperties();
	      		MaterialAvailabilityConfirmationProcessInformation mdtcObj = new MaterialAvailabilityConfirmationProcessInformation();
	      		mdtcObj.setAvailabilityConfirmationModeCode((String)mdtcData.get("AvailabilityConfirmationModeCode"));
	      		mdtcObj.setAvailabilityConfirmationModeCodeText((String)mdtcData.get("AvailabilityConfirmationModeCodeText"));
	      		mdtcObj.setSupplyPlanningAreaID((String)mdtcData.get("SupplyPlanningAreaID"));
	      		mdtcObj.setLifeCycleStatusCode((String)mdtcData.get("LifeCycleStatusCode"));
	      		mdtcObj.setLifeCycleStatusCodeText((String)mdtcData.get("LifeCycleStatusCodeText"));
	      		
	      		materialAvailabilityConfirmationProcessInformationService.save(mdtcObj);
	      		materialAvailabilityConfirmationProcessInformation.add(mdtcObj);
	      	}
	      	// End MaterialAvailabilityConfirmationProcessInformation
	      	
	      	materialService.save(m);
        }
        return ResponseEntity.ok().build();
    }
    
    /**
     * {@code PUT  /materials} : Updates an existing material.
     *
     * @param material the material to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated material,
     * or with status {@code 400 (Bad Request)} if the material is not valid,
     * or with status {@code 500 (Internal Server Error)} if the material couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/materials")
    public ResponseEntity<Material> updateMaterial(@RequestBody Material material) throws URISyntaxException {
        log.debug("REST request to update Material : {}", material);
        if (material.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Material result = materialService.save(material);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, material.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /materials} : get all the materials.
     *
     * @param pageable the pagination information.
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materials in body.
     */
    @GetMapping("/materials")
    public ResponseEntity<List<Material>> getAllMaterials(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder, @RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get a page of Materials");
        Page<Material> page;
        if (eagerload) {
            page = materialService.findAllWithEagerRelationships(pageable);
        } else {
            page = materialService.findAll(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /materials/:id} : get the "id" material.
     *
     * @param id the id of the material to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the material, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/materials/{id}")
    public ResponseEntity<Material> getMaterial(@PathVariable Long id) {
        log.debug("REST request to get Material : {}", id);
        Optional<Material> material = materialService.findOne(id);
        return ResponseUtil.wrapOrNotFound(material);
    }

    /**
     * {@code DELETE  /materials/:id} : delete the "id" material.
     *
     * @param id the id of the material to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/materials/{id}")
    public ResponseEntity<Void> deleteMaterial(@PathVariable Long id) {
        log.debug("REST request to delete Material : {}", id);
        materialService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
    

    public static final String HTTP_METHOD_PUT = "PUT";
    public static final String HTTP_METHOD_POST = "POST";
    public static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_DELETE = "DELETE";

    public static final String HTTP_HEADER_CONTENT_TYPE = "Content-Type";
    public static final String HTTP_HEADER_ACCEPT = "Accept";

    public static final String APPLICATION_JSON = "application/json";
    public static final String APPLICATION_XML = "application/xml";
    public static final String APPLICATION_ATOM_XML = "application/atom+xml";
    public static final String APPLICATION_FORM = "application/x-www-form-urlencoded";
    public static final String METADATA = "$metadata";
    public static final String INDEX = "/index.jsp";
    public static final String SEPARATOR = "/";

    public static final boolean PRINT_RAW_CONTENT = true;
    
	  public Edm readEdm(String serviceUrl) throws IOException, ODataException {
	    InputStream content = execute(serviceUrl + SEPARATOR + METADATA, APPLICATION_XML, HTTP_METHOD_GET);
	    return EntityProvider.readMetadata(content, false);
	  }

	  public ODataFeed readFeed(Edm edm, String serviceUri, String contentType, String entitySetName)
	      throws IOException, ODataException {
	    EdmEntityContainer entityContainer = edm.getDefaultEntityContainer();
	    String absolutUri = createUri(serviceUri, entitySetName, null );
//	    InputStream content = execute(absolutUri, contentType, HTTP_METHOD_GET);
	    
	    InputStream content = execute("https://my348779.sapbydesign.com/sap/byd/odata/cust/v1/vmumaterial/MaterialCollection?$top=1", contentType, HTTP_METHOD_GET);
	    
	   
	    return EntityProvider.readFeed(contentType,
	        entityContainer.getEntitySet(entitySetName),
	        content,
	        EntityProviderReadProperties.init().build());
	  }

	  private String createUri(String serviceUri, String entitySetName, String id) {
  	    return createUri(serviceUri, entitySetName, id, null);
  	  }

  	  private String createUri(String serviceUri, String entitySetName, String id, String expand) {
  	    final StringBuilder absolutUri = new StringBuilder(serviceUri).append(SEPARATOR).append(entitySetName);
  	    if(id != null) {
  	      absolutUri.append("(").append(id).append(")");
  	    }
  	    if(expand != null) {
  	      absolutUri.append("/?$expand=").append(expand);
  	    }
  	    return absolutUri.toString();
  	  }

  	  private InputStream execute(String relativeUri, String contentType, String httpMethod) throws IOException {
  	    HttpURLConnection connection = initializeConnection(relativeUri, contentType, httpMethod);
  	    connection.connect();
  	    checkStatus(connection);

  	    InputStream content = connection.getInputStream();
  	    content = logRawContent(httpMethod + " request on uri '" + relativeUri + "' with content:\n  ", content, "\n");
  	    return content;
  	  }

  	  private HttpURLConnection connect(String relativeUri, String contentType, String httpMethod) throws IOException {
  	    HttpURLConnection connection = initializeConnection(relativeUri, contentType, httpMethod);

  	    connection.connect();
  	    checkStatus(connection);

  	    return connection;
  	  }

  	  private HttpURLConnection initializeConnection(String absolutUri, String contentType, String httpMethod)
  	      throws MalformedURLException, IOException {
  	    URL url = new URL(absolutUri);
  	    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
  	    String basicAuth = "Basic QU5KQVIwMTpXZWxjb21lMQ==";
  	    connection.setRequestProperty ("Authorization", basicAuth);
  	    connection.setRequestProperty("x-csrf-token", "fetch");
  	    connection.setRequestMethod(httpMethod);
  	    connection.setRequestProperty(HTTP_HEADER_ACCEPT, contentType);
  	    if(HTTP_METHOD_POST.equals(httpMethod) || HTTP_METHOD_PUT.equals(httpMethod)) {
  	      connection.setDoOutput(true);
  	      connection.setRequestProperty(HTTP_HEADER_CONTENT_TYPE, contentType);
  	    }

  	    return connection;
  	  }
  	  

	  private HttpStatusCodes checkStatus(HttpURLConnection connection) throws IOException {    		  
	    HttpStatusCodes httpStatusCode = HttpStatusCodes.fromStatusCode(connection.getResponseCode());
	    if (400 <= httpStatusCode.getStatusCode() && httpStatusCode.getStatusCode() <= 599) {
	      throw new RuntimeException("Http Connection failed with status " + httpStatusCode.getStatusCode() + " " + httpStatusCode.toString());
	    }
	    return httpStatusCode;
	  }

	  private InputStream logRawContent(String prefix, InputStream content, String postfix) throws IOException {
  	    if(PRINT_RAW_CONTENT) {
  	      byte[] buffer = streamToArray(content);
  	      print(prefix + new String(buffer) + postfix);
  	      return new ByteArrayInputStream(buffer);
  	    }
  	    return content;
  	  }

	  private static void print(String content) {
        System.out.println(content);
      }
	  
  	  private byte[] streamToArray(InputStream stream) throws IOException {
  	    byte[] result = new byte[0];
  	    byte[] tmp = new byte[8192];
  	    int readCount = stream.read(tmp);
  	    while(readCount >= 0) {
  	      byte[] innerTmp = new byte[result.length + readCount];
  	      System.arraycopy(result, 0, innerTmp, 0, result.length);
  	      System.arraycopy(tmp, 0, innerTmp, result.length, readCount);
  	      result = innerTmp;
  	      readCount = stream.read(tmp);
  	    }
  	    stream.close();
  	    return result;
  	  }
}
