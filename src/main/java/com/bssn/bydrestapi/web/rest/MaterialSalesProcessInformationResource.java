package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialSalesProcessInformation;
import com.bssn.bydrestapi.service.MaterialSalesProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialSalesProcessInformation}.
 */
@RestController
@RequestMapping("/api")
public class MaterialSalesProcessInformationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialSalesProcessInformationResource.class);

    private static final String ENTITY_NAME = "materialSalesProcessInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialSalesProcessInformationService materialSalesProcessInformationService;

    public MaterialSalesProcessInformationResource(MaterialSalesProcessInformationService materialSalesProcessInformationService) {
        this.materialSalesProcessInformationService = materialSalesProcessInformationService;
    }

    /**
     * {@code POST  /material-sales-process-informations} : Create a new materialSalesProcessInformation.
     *
     * @param materialSalesProcessInformation the materialSalesProcessInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialSalesProcessInformation, or with status {@code 400 (Bad Request)} if the materialSalesProcessInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-sales-process-informations")
    public ResponseEntity<MaterialSalesProcessInformation> createMaterialSalesProcessInformation(@RequestBody MaterialSalesProcessInformation materialSalesProcessInformation) throws URISyntaxException {
        log.debug("REST request to save MaterialSalesProcessInformation : {}", materialSalesProcessInformation);
        if (materialSalesProcessInformation.getId() != null) {
            throw new BadRequestAlertException("A new materialSalesProcessInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialSalesProcessInformation result = materialSalesProcessInformationService.save(materialSalesProcessInformation);
        return ResponseEntity.created(new URI("/api/material-sales-process-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-sales-process-informations} : Updates an existing materialSalesProcessInformation.
     *
     * @param materialSalesProcessInformation the materialSalesProcessInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialSalesProcessInformation,
     * or with status {@code 400 (Bad Request)} if the materialSalesProcessInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialSalesProcessInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-sales-process-informations")
    public ResponseEntity<MaterialSalesProcessInformation> updateMaterialSalesProcessInformation(@RequestBody MaterialSalesProcessInformation materialSalesProcessInformation) throws URISyntaxException {
        log.debug("REST request to update MaterialSalesProcessInformation : {}", materialSalesProcessInformation);
        if (materialSalesProcessInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialSalesProcessInformation result = materialSalesProcessInformationService.save(materialSalesProcessInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialSalesProcessInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-sales-process-informations} : get all the materialSalesProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialSalesProcessInformations in body.
     */
    @GetMapping("/material-sales-process-informations")
    public ResponseEntity<List<MaterialSalesProcessInformation>> getAllMaterialSalesProcessInformations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialSalesProcessInformations");
        Page<MaterialSalesProcessInformation> page = materialSalesProcessInformationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-sales-process-informations/:id} : get the "id" materialSalesProcessInformation.
     *
     * @param id the id of the materialSalesProcessInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialSalesProcessInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-sales-process-informations/{id}")
    public ResponseEntity<MaterialSalesProcessInformation> getMaterialSalesProcessInformation(@PathVariable Long id) {
        log.debug("REST request to get MaterialSalesProcessInformation : {}", id);
        Optional<MaterialSalesProcessInformation> materialSalesProcessInformation = materialSalesProcessInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialSalesProcessInformation);
    }

    /**
     * {@code DELETE  /material-sales-process-informations/:id} : delete the "id" materialSalesProcessInformation.
     *
     * @param id the id of the materialSalesProcessInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-sales-process-informations/{id}")
    public ResponseEntity<Void> deleteMaterialSalesProcessInformation(@PathVariable Long id) {
        log.debug("REST request to delete MaterialSalesProcessInformation : {}", id);
        materialSalesProcessInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
