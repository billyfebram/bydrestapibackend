package com.bssn.bydrestapi.web.rest;

import com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation;
import com.bssn.bydrestapi.service.MaterialAvailabilityConfirmationProcessInformationService;
import com.bssn.bydrestapi.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.util.MultiValueMap;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.bssn.bydrestapi.domain.MaterialAvailabilityConfirmationProcessInformation}.
 */
@RestController
@RequestMapping("/api")
public class MaterialAvailabilityConfirmationProcessInformationResource {

    private final Logger log = LoggerFactory.getLogger(MaterialAvailabilityConfirmationProcessInformationResource.class);

    private static final String ENTITY_NAME = "materialAvailabilityConfirmationProcessInformation";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MaterialAvailabilityConfirmationProcessInformationService materialAvailabilityConfirmationProcessInformationService;

    public MaterialAvailabilityConfirmationProcessInformationResource(MaterialAvailabilityConfirmationProcessInformationService materialAvailabilityConfirmationProcessInformationService) {
        this.materialAvailabilityConfirmationProcessInformationService = materialAvailabilityConfirmationProcessInformationService;
    }

    /**
     * {@code POST  /material-availability-confirmation-process-informations} : Create a new materialAvailabilityConfirmationProcessInformation.
     *
     * @param materialAvailabilityConfirmationProcessInformation the materialAvailabilityConfirmationProcessInformation to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new materialAvailabilityConfirmationProcessInformation, or with status {@code 400 (Bad Request)} if the materialAvailabilityConfirmationProcessInformation has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/material-availability-confirmation-process-informations")
    public ResponseEntity<MaterialAvailabilityConfirmationProcessInformation> createMaterialAvailabilityConfirmationProcessInformation(@RequestBody MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation) throws URISyntaxException {
        log.debug("REST request to save MaterialAvailabilityConfirmationProcessInformation : {}", materialAvailabilityConfirmationProcessInformation);
        if (materialAvailabilityConfirmationProcessInformation.getId() != null) {
            throw new BadRequestAlertException("A new materialAvailabilityConfirmationProcessInformation cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MaterialAvailabilityConfirmationProcessInformation result = materialAvailabilityConfirmationProcessInformationService.save(materialAvailabilityConfirmationProcessInformation);
        return ResponseEntity.created(new URI("/api/material-availability-confirmation-process-informations/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /material-availability-confirmation-process-informations} : Updates an existing materialAvailabilityConfirmationProcessInformation.
     *
     * @param materialAvailabilityConfirmationProcessInformation the materialAvailabilityConfirmationProcessInformation to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated materialAvailabilityConfirmationProcessInformation,
     * or with status {@code 400 (Bad Request)} if the materialAvailabilityConfirmationProcessInformation is not valid,
     * or with status {@code 500 (Internal Server Error)} if the materialAvailabilityConfirmationProcessInformation couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/material-availability-confirmation-process-informations")
    public ResponseEntity<MaterialAvailabilityConfirmationProcessInformation> updateMaterialAvailabilityConfirmationProcessInformation(@RequestBody MaterialAvailabilityConfirmationProcessInformation materialAvailabilityConfirmationProcessInformation) throws URISyntaxException {
        log.debug("REST request to update MaterialAvailabilityConfirmationProcessInformation : {}", materialAvailabilityConfirmationProcessInformation);
        if (materialAvailabilityConfirmationProcessInformation.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MaterialAvailabilityConfirmationProcessInformation result = materialAvailabilityConfirmationProcessInformationService.save(materialAvailabilityConfirmationProcessInformation);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, materialAvailabilityConfirmationProcessInformation.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /material-availability-confirmation-process-informations} : get all the materialAvailabilityConfirmationProcessInformations.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of materialAvailabilityConfirmationProcessInformations in body.
     */
    @GetMapping("/material-availability-confirmation-process-informations")
    public ResponseEntity<List<MaterialAvailabilityConfirmationProcessInformation>> getAllMaterialAvailabilityConfirmationProcessInformations(Pageable pageable, @RequestParam MultiValueMap<String, String> queryParams, UriComponentsBuilder uriBuilder) {
        log.debug("REST request to get a page of MaterialAvailabilityConfirmationProcessInformations");
        Page<MaterialAvailabilityConfirmationProcessInformation> page = materialAvailabilityConfirmationProcessInformationService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(uriBuilder.queryParams(queryParams), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /material-availability-confirmation-process-informations/:id} : get the "id" materialAvailabilityConfirmationProcessInformation.
     *
     * @param id the id of the materialAvailabilityConfirmationProcessInformation to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the materialAvailabilityConfirmationProcessInformation, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/material-availability-confirmation-process-informations/{id}")
    public ResponseEntity<MaterialAvailabilityConfirmationProcessInformation> getMaterialAvailabilityConfirmationProcessInformation(@PathVariable Long id) {
        log.debug("REST request to get MaterialAvailabilityConfirmationProcessInformation : {}", id);
        Optional<MaterialAvailabilityConfirmationProcessInformation> materialAvailabilityConfirmationProcessInformation = materialAvailabilityConfirmationProcessInformationService.findOne(id);
        return ResponseUtil.wrapOrNotFound(materialAvailabilityConfirmationProcessInformation);
    }

    /**
     * {@code DELETE  /material-availability-confirmation-process-informations/:id} : delete the "id" materialAvailabilityConfirmationProcessInformation.
     *
     * @param id the id of the materialAvailabilityConfirmationProcessInformation to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/material-availability-confirmation-process-informations/{id}")
    public ResponseEntity<Void> deleteMaterialAvailabilityConfirmationProcessInformation(@PathVariable Long id) {
        log.debug("REST request to delete MaterialAvailabilityConfirmationProcessInformation : {}", id);
        materialAvailabilityConfirmationProcessInformationService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id.toString())).build();
    }
}
