package com.bssn.bydrestapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialInventoryProcessInformation.
 */
@Entity
@Table(name = "m_inventory_process_info")
public class MaterialInventoryProcessInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "site_id")
    private String siteID;

    @Column(name = "life_cycle_status_code")
    private String lifeCycleStatusCode;

    @Column(name = "life_cycle_status_code_text")
    private String lifeCycleStatusCodeText;

    @ManyToMany(mappedBy = "materialInventoryProcessInformations")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSiteID() {
        return siteID;
    }

    public MaterialInventoryProcessInformation siteID(String siteID) {
        this.siteID = siteID;
        return this;
    }

    public void setSiteID(String siteID) {
        this.siteID = siteID;
    }

    public String getLifeCycleStatusCode() {
        return lifeCycleStatusCode;
    }

    public MaterialInventoryProcessInformation lifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
        return this;
    }

    public void setLifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
    }

    public String getLifeCycleStatusCodeText() {
        return lifeCycleStatusCodeText;
    }

    public MaterialInventoryProcessInformation lifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
        return this;
    }

    public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialInventoryProcessInformation materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialInventoryProcessInformation addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialInventoryProcessInformations().add(this);
        return this;
    }

    public MaterialInventoryProcessInformation removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialInventoryProcessInformations().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialInventoryProcessInformation)) {
            return false;
        }
        return id != null && id.equals(((MaterialInventoryProcessInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialInventoryProcessInformation{" +
            "id=" + getId() +
            ", siteID='" + getSiteID() + "'" +
            ", lifeCycleStatusCode='" + getLifeCycleStatusCode() + "'" +
            ", lifeCycleStatusCodeText='" + getLifeCycleStatusCodeText() + "'" +
            "}";
    }
}
