package com.bssn.bydrestapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialCrossProcessCategory.
 */
@Entity
@Table(name = "m_cross_process_category")
public class MaterialCrossProcessCategory implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_category_internal_id")
    private String productCategoryInternalID;

    @Column(name = "description")
    private String description;

    @Column(name = "description_language_code")
    private String descriptionLanguageCode;

    @Column(name = "description_language_code_text")
    private String descriptionLanguageCodeText;

    @ManyToMany(mappedBy = "materialCrossProcessCategories")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductCategoryInternalID() {
        return productCategoryInternalID;
    }

    public MaterialCrossProcessCategory productCategoryInternalID(String productCategoryInternalID) {
        this.productCategoryInternalID = productCategoryInternalID;
        return this;
    }

    public void setProductCategoryInternalID(String productCategoryInternalID) {
        this.productCategoryInternalID = productCategoryInternalID;
    }

    public String getDescription() {
        return description;
    }

    public MaterialCrossProcessCategory description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionLanguageCode() {
        return descriptionLanguageCode;
    }

    public MaterialCrossProcessCategory descriptionLanguageCode(String descriptionLanguageCode) {
        this.descriptionLanguageCode = descriptionLanguageCode;
        return this;
    }

    public void setDescriptionLanguageCode(String descriptionLanguageCode) {
        this.descriptionLanguageCode = descriptionLanguageCode;
    }

    public String getDescriptionLanguageCodeText() {
        return descriptionLanguageCodeText;
    }

    public MaterialCrossProcessCategory descriptionLanguageCodeText(String descriptionLanguageCodeText) {
        this.descriptionLanguageCodeText = descriptionLanguageCodeText;
        return this;
    }

    public void setDescriptionLanguageCodeText(String descriptionLanguageCodeText) {
        this.descriptionLanguageCodeText = descriptionLanguageCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialCrossProcessCategory materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialCrossProcessCategory addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialCrossProcessCategories().add(this);
        return this;
    }

    public MaterialCrossProcessCategory removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialCrossProcessCategories().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialCrossProcessCategory)) {
            return false;
        }
        return id != null && id.equals(((MaterialCrossProcessCategory) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialCrossProcessCategory{" +
            "id=" + getId() +
            ", productCategoryInternalID='" + getProductCategoryInternalID() + "'" +
            ", description='" + getDescription() + "'" +
            ", descriptionLanguageCode='" + getDescriptionLanguageCode() + "'" +
            ", descriptionLanguageCodeText='" + getDescriptionLanguageCodeText() + "'" +
            "}";
    }
}
