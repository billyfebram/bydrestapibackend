package com.bssn.bydrestapi.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A MaterialSupplyPlanningProcessInformation.
 */
@Entity
@Table(name = "m_supply_planning_info")
public class MaterialSupplyPlanningProcessInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "default_procurement_method_code")
    private String defaultProcurementMethodCode;

    @Column(name = "default_procurement_method_code_text")
    private String defaultProcurementMethodCodeText;

    @Column(name = "supply_planning_procedure_code")
    private String supplyPlanningProcedureCode;

    @Column(name = "supply_planning_procedure_code_text")
    private String supplyPlanningProcedureCodeText;

    @Column(name = "lot_size_procedure_code")
    private String lotSizeProcedureCode;

    @Column(name = "lot_size_procedure_code_text")
    private String lotSizeProcedureCodeText;

    @Column(name = "supply_planning_area_id")
    private String supplyPlanningAreaID;

    @Column(name = "life_cycle_status_code")
    private String lifeCycleStatusCode;

    @Column(name = "life_cycle_status_code_text")
    private String lifeCycleStatusCodeText;

    @ManyToMany(mappedBy = "materialSupplyPlanningProcessInformations")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDefaultProcurementMethodCode() {
        return defaultProcurementMethodCode;
    }

    public MaterialSupplyPlanningProcessInformation defaultProcurementMethodCode(String defaultProcurementMethodCode) {
        this.defaultProcurementMethodCode = defaultProcurementMethodCode;
        return this;
    }

    public void setDefaultProcurementMethodCode(String defaultProcurementMethodCode) {
        this.defaultProcurementMethodCode = defaultProcurementMethodCode;
    }

    public String getDefaultProcurementMethodCodeText() {
        return defaultProcurementMethodCodeText;
    }

    public MaterialSupplyPlanningProcessInformation defaultProcurementMethodCodeText(String defaultProcurementMethodCodeText) {
        this.defaultProcurementMethodCodeText = defaultProcurementMethodCodeText;
        return this;
    }

    public void setDefaultProcurementMethodCodeText(String defaultProcurementMethodCodeText) {
        this.defaultProcurementMethodCodeText = defaultProcurementMethodCodeText;
    }

    public String getSupplyPlanningProcedureCode() {
        return supplyPlanningProcedureCode;
    }

    public MaterialSupplyPlanningProcessInformation supplyPlanningProcedureCode(String supplyPlanningProcedureCode) {
        this.supplyPlanningProcedureCode = supplyPlanningProcedureCode;
        return this;
    }

    public void setSupplyPlanningProcedureCode(String supplyPlanningProcedureCode) {
        this.supplyPlanningProcedureCode = supplyPlanningProcedureCode;
    }

    public String getSupplyPlanningProcedureCodeText() {
        return supplyPlanningProcedureCodeText;
    }

    public MaterialSupplyPlanningProcessInformation supplyPlanningProcedureCodeText(String supplyPlanningProcedureCodeText) {
        this.supplyPlanningProcedureCodeText = supplyPlanningProcedureCodeText;
        return this;
    }

    public void setSupplyPlanningProcedureCodeText(String supplyPlanningProcedureCodeText) {
        this.supplyPlanningProcedureCodeText = supplyPlanningProcedureCodeText;
    }

    public String getLotSizeProcedureCode() {
        return lotSizeProcedureCode;
    }

    public MaterialSupplyPlanningProcessInformation lotSizeProcedureCode(String lotSizeProcedureCode) {
        this.lotSizeProcedureCode = lotSizeProcedureCode;
        return this;
    }

    public void setLotSizeProcedureCode(String lotSizeProcedureCode) {
        this.lotSizeProcedureCode = lotSizeProcedureCode;
    }

    public String getLotSizeProcedureCodeText() {
        return lotSizeProcedureCodeText;
    }

    public MaterialSupplyPlanningProcessInformation lotSizeProcedureCodeText(String lotSizeProcedureCodeText) {
        this.lotSizeProcedureCodeText = lotSizeProcedureCodeText;
        return this;
    }

    public void setLotSizeProcedureCodeText(String lotSizeProcedureCodeText) {
        this.lotSizeProcedureCodeText = lotSizeProcedureCodeText;
    }

    public String getSupplyPlanningAreaID() {
        return supplyPlanningAreaID;
    }

    public MaterialSupplyPlanningProcessInformation supplyPlanningAreaID(String supplyPlanningAreaID) {
        this.supplyPlanningAreaID = supplyPlanningAreaID;
        return this;
    }

    public void setSupplyPlanningAreaID(String supplyPlanningAreaID) {
        this.supplyPlanningAreaID = supplyPlanningAreaID;
    }

    public String getLifeCycleStatusCode() {
        return lifeCycleStatusCode;
    }

    public MaterialSupplyPlanningProcessInformation lifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
        return this;
    }

    public void setLifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
    }

    public String getLifeCycleStatusCodeText() {
        return lifeCycleStatusCodeText;
    }

    public MaterialSupplyPlanningProcessInformation lifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
        return this;
    }

    public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialSupplyPlanningProcessInformation materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialSupplyPlanningProcessInformation addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialSupplyPlanningProcessInformations().add(this);
        return this;
    }

    public MaterialSupplyPlanningProcessInformation removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialSupplyPlanningProcessInformations().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialSupplyPlanningProcessInformation)) {
            return false;
        }
        return id != null && id.equals(((MaterialSupplyPlanningProcessInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialSupplyPlanningProcessInformation{" +
            "id=" + getId() +
            ", defaultProcurementMethodCode='" + getDefaultProcurementMethodCode() + "'" +
            ", defaultProcurementMethodCodeText='" + getDefaultProcurementMethodCodeText() + "'" +
            ", supplyPlanningProcedureCode='" + getSupplyPlanningProcedureCode() + "'" +
            ", supplyPlanningProcedureCodeText='" + getSupplyPlanningProcedureCodeText() + "'" +
            ", lotSizeProcedureCode='" + getLotSizeProcedureCode() + "'" +
            ", lotSizeProcedureCodeText='" + getLotSizeProcedureCodeText() + "'" +
            ", supplyPlanningAreaID='" + getSupplyPlanningAreaID() + "'" +
            ", lifeCycleStatusCode='" + getLifeCycleStatusCode() + "'" +
            ", lifeCycleStatusCodeText='" + getLifeCycleStatusCodeText() + "'" +
            "}";
    }
}
