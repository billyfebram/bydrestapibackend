package com.bssn.bydrestapi.domain;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A MaterialSalesProcessInformation.
 */
@Entity
@Table(name = "m_sales_process_info")
public class MaterialSalesProcessInformation implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sales_organisation_id")
    private String salesOrganisationID;

    @Column(name = "distribution_channel_code")
    private String distributionChannelCode;

    @Column(name = "distribution_channel_code_text")
    private String distributionChannelCodeText;

    @Column(name = "life_cycle_status_code")
    private String lifeCycleStatusCode;

    @Column(name = "life_cycle_status_code_text")
    private String lifeCycleStatusCodeText;

    @Column(name = "sales_measure_unit_code")
    private String salesMeasureUnitCode;

    @Column(name = "sales_measure_unit_code_text")
    private String salesMeasureUnitCodeText;

    @Column(name = "cust_trx_doc_item_process_group_code")
    private String customerTrxDocItemProcessingTypeDeterminationProductGroupCode;

    @Column(name = "cust_trx_doc_item_process_group_code_txt")
    private String customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt;

    @Column(name = "cash_discount_deductible_indicator")
    private Boolean cashDiscountDeductibleIndicator;

    @Column(name = "minimum_order_quantity")
    private BigDecimal minimumOrderQuantity;

    @ManyToMany(mappedBy = "materialSalesProcessInformations")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSalesOrganisationID() {
        return salesOrganisationID;
    }

    public MaterialSalesProcessInformation salesOrganisationID(String salesOrganisationID) {
        this.salesOrganisationID = salesOrganisationID;
        return this;
    }

    public void setSalesOrganisationID(String salesOrganisationID) {
        this.salesOrganisationID = salesOrganisationID;
    }

    public String getDistributionChannelCode() {
        return distributionChannelCode;
    }

    public MaterialSalesProcessInformation distributionChannelCode(String distributionChannelCode) {
        this.distributionChannelCode = distributionChannelCode;
        return this;
    }

    public void setDistributionChannelCode(String distributionChannelCode) {
        this.distributionChannelCode = distributionChannelCode;
    }

    public String getDistributionChannelCodeText() {
        return distributionChannelCodeText;
    }

    public MaterialSalesProcessInformation distributionChannelCodeText(String distributionChannelCodeText) {
        this.distributionChannelCodeText = distributionChannelCodeText;
        return this;
    }

    public void setDistributionChannelCodeText(String distributionChannelCodeText) {
        this.distributionChannelCodeText = distributionChannelCodeText;
    }

    public String getLifeCycleStatusCode() {
        return lifeCycleStatusCode;
    }

    public MaterialSalesProcessInformation lifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
        return this;
    }

    public void setLifeCycleStatusCode(String lifeCycleStatusCode) {
        this.lifeCycleStatusCode = lifeCycleStatusCode;
    }

    public String getLifeCycleStatusCodeText() {
        return lifeCycleStatusCodeText;
    }

    public MaterialSalesProcessInformation lifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
        return this;
    }

    public void setLifeCycleStatusCodeText(String lifeCycleStatusCodeText) {
        this.lifeCycleStatusCodeText = lifeCycleStatusCodeText;
    }

    public String getSalesMeasureUnitCode() {
        return salesMeasureUnitCode;
    }

    public MaterialSalesProcessInformation salesMeasureUnitCode(String salesMeasureUnitCode) {
        this.salesMeasureUnitCode = salesMeasureUnitCode;
        return this;
    }

    public void setSalesMeasureUnitCode(String salesMeasureUnitCode) {
        this.salesMeasureUnitCode = salesMeasureUnitCode;
    }

    public String getSalesMeasureUnitCodeText() {
        return salesMeasureUnitCodeText;
    }

    public MaterialSalesProcessInformation salesMeasureUnitCodeText(String salesMeasureUnitCodeText) {
        this.salesMeasureUnitCodeText = salesMeasureUnitCodeText;
        return this;
    }

    public void setSalesMeasureUnitCodeText(String salesMeasureUnitCodeText) {
        this.salesMeasureUnitCodeText = salesMeasureUnitCodeText;
    }

    public String getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode() {
        return customerTrxDocItemProcessingTypeDeterminationProductGroupCode;
    }

    public MaterialSalesProcessInformation customerTrxDocItemProcessingTypeDeterminationProductGroupCode(String customerTrxDocItemProcessingTypeDeterminationProductGroupCode) {
        this.customerTrxDocItemProcessingTypeDeterminationProductGroupCode = customerTrxDocItemProcessingTypeDeterminationProductGroupCode;
        return this;
    }

    public void setCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode(String customerTrxDocItemProcessingTypeDeterminationProductGroupCode) {
        this.customerTrxDocItemProcessingTypeDeterminationProductGroupCode = customerTrxDocItemProcessingTypeDeterminationProductGroupCode;
    }

    public String getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt() {
        return customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt;
    }

    public MaterialSalesProcessInformation customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt(String customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt) {
        this.customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt = customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt;
        return this;
    }

    public void setCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt(String customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt) {
        this.customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt = customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt;
    }

    public Boolean isCashDiscountDeductibleIndicator() {
        return cashDiscountDeductibleIndicator;
    }

    public MaterialSalesProcessInformation cashDiscountDeductibleIndicator(Boolean cashDiscountDeductibleIndicator) {
        this.cashDiscountDeductibleIndicator = cashDiscountDeductibleIndicator;
        return this;
    }

    public void setCashDiscountDeductibleIndicator(Boolean cashDiscountDeductibleIndicator) {
        this.cashDiscountDeductibleIndicator = cashDiscountDeductibleIndicator;
    }

    public BigDecimal getMinimumOrderQuantity() {
        return minimumOrderQuantity;
    }

    public MaterialSalesProcessInformation minimumOrderQuantity(BigDecimal minimumOrderQuantity) {
        this.minimumOrderQuantity = minimumOrderQuantity;
        return this;
    }

    public void setMinimumOrderQuantity(BigDecimal minimumOrderQuantity) {
        this.minimumOrderQuantity = minimumOrderQuantity;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialSalesProcessInformation materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialSalesProcessInformation addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialSalesProcessInformations().add(this);
        return this;
    }

    public MaterialSalesProcessInformation removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialSalesProcessInformations().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialSalesProcessInformation)) {
            return false;
        }
        return id != null && id.equals(((MaterialSalesProcessInformation) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialSalesProcessInformation{" +
            "id=" + getId() +
            ", salesOrganisationID='" + getSalesOrganisationID() + "'" +
            ", distributionChannelCode='" + getDistributionChannelCode() + "'" +
            ", distributionChannelCodeText='" + getDistributionChannelCodeText() + "'" +
            ", lifeCycleStatusCode='" + getLifeCycleStatusCode() + "'" +
            ", lifeCycleStatusCodeText='" + getLifeCycleStatusCodeText() + "'" +
            ", salesMeasureUnitCode='" + getSalesMeasureUnitCode() + "'" +
            ", salesMeasureUnitCodeText='" + getSalesMeasureUnitCodeText() + "'" +
            ", customerTrxDocItemProcessingTypeDeterminationProductGroupCode='" + getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCode() + "'" +
            ", customerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt='" + getCustomerTrxDocItemProcessingTypeDeterminationProductGroupCodeTxt() + "'" +
            ", cashDiscountDeductibleIndicator='" + isCashDiscountDeductibleIndicator() + "'" +
            ", minimumOrderQuantity=" + getMinimumOrderQuantity() +
            "}";
    }
}
