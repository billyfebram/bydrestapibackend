package com.bssn.bydrestapi.domain;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * A MaterialQuantityConversion.
 */
@Entity
@Table(name = "material_quantity_conversion")
public class MaterialQuantityConversion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "corresponding_quantity")
    private BigDecimal correspondingQuantity;

    @Column(name = "corresponding_quantity_unit_code")
    private String correspondingQuantityUnitCode;

    @Column(name = "corresponding_quantity_unit_code_text")
    private String correspondingQuantityUnitCodeText;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @Column(name = "quantity_unit_code")
    private String quantityUnitCode;

    @Column(name = "quantity_unit_code_text")
    private String quantityUnitCodeText;

    @Column(name = "batch_dependent_indicator")
    private Boolean batchDependentIndicator;

    @ManyToMany(mappedBy = "materialQuantityConversions")
    @JsonIgnore
    private Set<Material> materials = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getCorrespondingQuantity() {
        return correspondingQuantity;
    }

    public MaterialQuantityConversion correspondingQuantity(BigDecimal correspondingQuantity) {
        this.correspondingQuantity = correspondingQuantity;
        return this;
    }

    public void setCorrespondingQuantity(BigDecimal correspondingQuantity) {
        this.correspondingQuantity = correspondingQuantity;
    }

    public String getCorrespondingQuantityUnitCode() {
        return correspondingQuantityUnitCode;
    }

    public MaterialQuantityConversion correspondingQuantityUnitCode(String correspondingQuantityUnitCode) {
        this.correspondingQuantityUnitCode = correspondingQuantityUnitCode;
        return this;
    }

    public void setCorrespondingQuantityUnitCode(String correspondingQuantityUnitCode) {
        this.correspondingQuantityUnitCode = correspondingQuantityUnitCode;
    }

    public String getCorrespondingQuantityUnitCodeText() {
        return correspondingQuantityUnitCodeText;
    }

    public MaterialQuantityConversion correspondingQuantityUnitCodeText(String correspondingQuantityUnitCodeText) {
        this.correspondingQuantityUnitCodeText = correspondingQuantityUnitCodeText;
        return this;
    }

    public void setCorrespondingQuantityUnitCodeText(String correspondingQuantityUnitCodeText) {
        this.correspondingQuantityUnitCodeText = correspondingQuantityUnitCodeText;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public MaterialQuantityConversion quantity(BigDecimal quantity) {
        this.quantity = quantity;
        return this;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUnitCode() {
        return quantityUnitCode;
    }

    public MaterialQuantityConversion quantityUnitCode(String quantityUnitCode) {
        this.quantityUnitCode = quantityUnitCode;
        return this;
    }

    public void setQuantityUnitCode(String quantityUnitCode) {
        this.quantityUnitCode = quantityUnitCode;
    }

    public String getQuantityUnitCodeText() {
        return quantityUnitCodeText;
    }

    public MaterialQuantityConversion quantityUnitCodeText(String quantityUnitCodeText) {
        this.quantityUnitCodeText = quantityUnitCodeText;
        return this;
    }

    public void setQuantityUnitCodeText(String quantityUnitCodeText) {
        this.quantityUnitCodeText = quantityUnitCodeText;
    }

    public Boolean isBatchDependentIndicator() {
        return batchDependentIndicator;
    }

    public MaterialQuantityConversion batchDependentIndicator(Boolean batchDependentIndicator) {
        this.batchDependentIndicator = batchDependentIndicator;
        return this;
    }

    public void setBatchDependentIndicator(Boolean batchDependentIndicator) {
        this.batchDependentIndicator = batchDependentIndicator;
    }

    public Set<Material> getMaterials() {
        return materials;
    }

    public MaterialQuantityConversion materials(Set<Material> materials) {
        this.materials = materials;
        return this;
    }

    public MaterialQuantityConversion addMaterial(Material material) {
        this.materials.add(material);
        material.getMaterialQuantityConversions().add(this);
        return this;
    }

    public MaterialQuantityConversion removeMaterial(Material material) {
        this.materials.remove(material);
        material.getMaterialQuantityConversions().remove(this);
        return this;
    }

    public void setMaterials(Set<Material> materials) {
        this.materials = materials;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MaterialQuantityConversion)) {
            return false;
        }
        return id != null && id.equals(((MaterialQuantityConversion) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "MaterialQuantityConversion{" +
            "id=" + getId() +
            ", correspondingQuantity=" + getCorrespondingQuantity() +
            ", correspondingQuantityUnitCode='" + getCorrespondingQuantityUnitCode() + "'" +
            ", correspondingQuantityUnitCodeText='" + getCorrespondingQuantityUnitCodeText() + "'" +
            ", quantity=" + getQuantity() +
            ", quantityUnitCode='" + getQuantityUnitCode() + "'" +
            ", quantityUnitCodeText='" + getQuantityUnitCodeText() + "'" +
            ", batchDependentIndicator='" + isBatchDependentIndicator() + "'" +
            "}";
    }
}
